﻿
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest.TestCases
{
//    [TestFixture]
    public class DummyTest : BaseClass
    {

        int RowCount;
        String Filepath = ExcelFilepath + "\\Data\\Services.xlsx";
        String Registrationpath = ExcelFilepath + "\\Data\\Registrationdata.xlsx";

        public object SeleniumExtras { get; private set; }

        [Test, Order(1), Category("SmokeTest")]
        public void ChangeAccountTest()
        {
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service1");

            for (int i = 2; i < RowCount; i++)
            {
                try
                {


                    var Pass = driver.FindElement(By.Id("MainContent_lblConfirm")).Text;
                    excelHelper.writedata(Filepath, "Service1", 16, i + 1, "Pass");
                    excelHelper.writedata(Filepath, "Service1", 17, i + 1, Pass);

                    test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

                }
                catch (Exception e)
                {
                    Console.WriteLine("hey catch here here");
                    var error = driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text;
                    excelHelper.writedata(Filepath, "Service1", 16, i + 1, "fail");
                    excelHelper.writedata(Filepath, "Service1", 17, i + 1, error);

                    Assert.Fail(e.Message);

                    test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                    test.Info(e);
                    test.AddScreenCaptureFromPath(Capture(driver));
                    continue;
                    // C# program to illustrate the use of 
                    // Split(Char[], StringSplitOptions) method 




                    // Taking a string 




                }
            }
        }


        [SetUp]
        public void BeforeTest()
        {
            int LogCount;

            LogCount = excelHelper.PopulateInCollecetion(Registrationpath, "LoginSheet");
            String user = excelHelper.ReadData(1, "Username");
            String pw = excelHelper.ReadData(1, "Password");
            loginmyclientpage.enterUsername(user);
            loginmyclientpage.enterPassword(pw);
            loginmyclientpage.clickSubmit(pw, test);
            test.AssignCategory("Smoke Test Suite of  " + MethodBase.GetCurrentMethod().DeclaringType.Name + " ");

        }
        [TearDown]
        public void clearresult()
        {
            excelHelper.datcollist.Clear();
            driver.Quit();

        }
    }
}
