﻿

using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest.TestCases
{
    [TestFixture]
    public class ApprovalRequestTransfer : BaseClass
    {

        int RowCount;
        String Filepath = ExcelFilepath + "\\Data\\Services.xlsx";
        String Registrationpath = ExcelFilepath + "\\Data\\Registrationdata.xlsx";


        public object SeleniumExtras { get; private set; }

        [Test, Order(1), Category("SmokeTest")]
        public void ApprovalRequestTransfer_test()
        {
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Approval request for transfer");

            for (int i = 1; i <= RowCount; i++)
            {
                String EstablishmentLabor = excelHelper.ReadData(i, "Establishment Labor office");
                String EstablishhmentNumber = excelHelper.ReadData(i, "Establishment Number");
                String EstablishmentRepresentative = excelHelper.ReadData(i, "Establishment representative id");
                String IDNumber = excelHelper.ReadData(i, "ID number");
                String Expected = excelHelper.ReadData(i, "Expected Result");
                String Action = excelHelper.ReadData(i, "Action");
                String Actual = excelHelper.ReadData(i, "Actual Result");
                try
                {



                    dashBoardPage.ClickTransferServicesSidePanel(test);
                    dashBoardPage.ClickTransferServicesChild(test);
                    dashBoardPage.ClickApproveDependantService(test);
                    approvalRequest.EnterEstablishmentLabor(EstablishmentLabor, test);
                    approvalRequest.EnterEstablishmentNumber(EstablishhmentNumber, test);
                    approvalRequest.EnterEstablishmentRepresentative(EstablishmentRepresentative, test);
                    approvalRequest.ClickCheckButton(test);
                    approvalRequest.EnterIdNumber(IDNumber, test);
                    approvalRequest.ErrorMethod(test);

                    switch (Action)
                    {
                        case "Approve":
                            approvalRequest.ClickApproveRequest(test);
                            break;

                        case "Reject":
                            approvalRequest.ClickRejectRequest(test);
                            break;


                    }


                    //var Pass = driver.FindElement(By.Id("MainContent_lblConfirm")).Text;
                    excelHelper.writedata(Filepath, "Approval request for transfer", 7, i + 1, "Pass");

                    //excelHelper.writedata(Filepath, "Service1", 17, i + 1, Pass);
                    //Assert.Pass();
                    test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");
                }

                catch (Exception e)
                {
                    try
                    {
                        Console.WriteLine("Parent Try");

                        HelperMethod.ExplicitWaitById(driver,"MainContent_IndividualSponsorTransfer_ucValidateBusinessUser_lblErrorMessage");
                       // var error = driver.FindElement(By.Id("MainContent_IndividualSponsorTransfer_divError"));
                        
                        var error = driver.FindElement(By.Id("MainContent_IndividualSponsorTransfer_ucValidateBusinessUser_lblErrorMessage"));

                        switch (error.Text)
                        {
                            case "نعتذر عن تنفيذ طلبك ، لقد تمت الموافقة على هذا الطلب من قبل":
                                excelHelper.writedata(Filepath, "Approval request for transfer", 6, i + 1, error.Text);
                                Console.WriteLine("1");

                                break;

                            case "لم يتم العثور على طلب يوافق مدخلات البحث":
                                excelHelper.writedata(Filepath, "Approval request for transfer", 6, i + 1, error.Text);
                                Console.WriteLine("2");

                                break;
                            case "نعتذر عن عدم تنفيذ طلبك، تراخيص المنشأة غير سارية (ترخيص السجل التجاري)":
                                excelHelper.writedata(Filepath, "Approval request for transfer", 6, i + 1, error.Text);
                                Console.WriteLine("3");

                                break;

                            case "تمت الموافقة على الطلب بنجاح":
                                excelHelper.writedata(Filepath, "Approval request for transfer", 6, i + 1, error.Text);
                                Console.WriteLine("4");

                                break;
                            case "تم رفض الطلب بنجاح":
                                excelHelper.writedata(Filepath, "Approval request for transfer", 6, i + 1, error.Text);
                                Console.WriteLine("5");

                                break;
                            case "لم يتم العثور على طالب الخدمة أو طالب الخدمة منتهى التصريح":
                                excelHelper.writedata(Filepath, "Approval request for transfer", 6, i + 1, error.Text);
                                Console.WriteLine("5");

                                break;
                            default:
                                excelHelper.writedata(Filepath, "Approval request for transfer", 6, i + 1, e.Message);
                                break;


                        }
                        if (Expected != Actual)

                        {

                            excelHelper.writedata(Filepath, "Approval request for transfer", 7, i + 1, "fail");
                            test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Fail ");


                        }
                        else if (Expected == Actual)
                        {
                            excelHelper.writedata(Filepath, "Approval request for transfer", 7, i + 1, "Pass");
                            test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");


                        }


                        test.Info(e);
                        continue;
                    }
                    catch (Exception m)
                    {
                        Console.WriteLine("Parent catch");

                        Console.WriteLine("NoSuchElementException");
                        excelHelper.writedata(Filepath, "Approval request for transfer", 7, i + 1, "fail");
                        excelHelper.writedata(Filepath, "Approval request for transfer", 6, i + 1, m.Message);

                        test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Fail ");
                        continue;

                    }
                    //test.AddScreenCaptureFromPath(Capture(driver));
                    continue;
                }

            }
        }


        [SetUp]
        public void BeforeTest()
        {
            int LogCount;

            LogCount = excelHelper.PopulateInCollecetion(Registrationpath, "LoginSheet");
            String user = excelHelper.ReadData(1, "Username");
            String pw = excelHelper.ReadData(1, "Password");
            loginmyclientpage.enterUsername(user);
            loginmyclientpage.enterPassword(pw);
            loginmyclientpage.clickSubmit(pw, test);
            test.AssignCategory("Smoke Test Suite of  " + MethodBase.GetCurrentMethod().DeclaringType.Name + " ");

        }
        [TearDown]
        public void clearresult()
        {
            excelHelper.datcollist.Clear();
            driver.Quit();

        }
    }
}
