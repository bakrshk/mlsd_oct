﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest.TestCases
{
   public class AddAccountManagerTest : BaseClass
    {
        int RowCount;
        String Filepath = ExcelFilepath + "\\Data\\Update.xlsx";


        //Add Account service al Mansha
        [Test, Order(2), Category("SmokeTest")]
        public void ACM_TC_04_AddNewUser()
        {

            // for (int i = 1; i <= RowCount; i++)
            //{
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");
            try
            {
                String user = excelHelper.ReadData(1, "Username");
                String pw = excelHelper.ReadData(1, "Password");
                String serviceid = excelHelper.ReadData(1, "Service Id");
                String unifiedid = excelHelper.ReadData(1, "Unified 1");
                String unifiedid2 = excelHelper.ReadData(1, "Unified 2");
                String fname = excelHelper.ReadData(1, "First Name");
                String lname = excelHelper.ReadData(1, "Last Name");
                String Nationality = excelHelper.ReadData(1, "Nationality");
                String calendertype = excelHelper.ReadData(1, "CalendarType");
                String year = excelHelper.ReadData(1, "Year");
                String month = excelHelper.ReadData(1, "Month");
                String date = excelHelper.ReadData(1, "Date");
                String id = excelHelper.ReadData(1, "New Unified ID");
                String Expected = excelHelper.ReadData(1, "Expected Result");


                //
                String facilitynum = "9";
                String facility_num2 = "1241";
                String facility_id = "1002000618";
                String optpNum = "796314";




                loginmyclientpage.enterUsername(user);
                loginmyclientpage.enterPassword(pw);
                loginmyclientpage.clickSubmit(pw, test);
                //dashBoardPage.clickManagementeEstablishments1();
                dashBoardPage.clickServicesEstablishment();
                dashBoardPage.clickaddAccountManager();
                addManager.enterServiceName(serviceid);
                //Add manager Page
                addManager.enterFacilityNum(facilitynum, facility_num2);
                addManager.enterFacilityIdNum(facility_id);
                addManager.clickSearchS2();
                addManager.clickOndate(calendertype, test);
                addManager.selectMonth(month, test);
                addManager.selectYear(year, test);
                addManager.selectDatelist(date);
                update.closeBtnDate();
                addManager.clickSaveBtn();
                addManager.enterOtpNum(optpNum);
                addManager.clickOtpBtn();

                Assert.Pass();
                //Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text);
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

            }

            catch (Exception e)
            {
                Console.WriteLine("hey catch here here");

                excelHelper.writedata(Filepath, "Service1", 17, 1, "Fail");

                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }
        }

//Add Same Account service al Mansha
        [Test, Order(3), Category("SmokeTest")]
        public void ACM_TC_05_addingSameUser()
        {

            // for (int i = 1; i <= RowCount; i++)
            //{
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");
            try
            {
                String user = excelHelper.ReadData(1, "Username");
                String pw = excelHelper.ReadData(1, "Password");
                String serviceid = excelHelper.ReadData(1, "Service Id");
                String unifiedid = excelHelper.ReadData(1, "Unified 1");
                String unifiedid2 = excelHelper.ReadData(1, "Unified 2");
                String fname = excelHelper.ReadData(1, "First Name");
                String lname = excelHelper.ReadData(1, "Last Name");
                String Nationality = excelHelper.ReadData(1, "Nationality");
                String calendertype = excelHelper.ReadData(1, "CalendarType");
                String year = excelHelper.ReadData(1, "Year");
                String month = excelHelper.ReadData(1, "Month");
                String date = excelHelper.ReadData(1, "Date");
                String id = excelHelper.ReadData(1, "New Unified ID");
                String Expected = excelHelper.ReadData(1, "Expected Result");


                //
                String facilitynum = "9";
                String facility_num2 = "1241";
                String facility_id = "1002000618";
                String optpNum = "796314";




                loginmyclientpage.enterUsername(user);
                loginmyclientpage.enterPassword(pw);
                loginmyclientpage.clickSubmit(pw, test);
                //dashBoardPage.clickManagementeEstablishments1();
                dashBoardPage.clickServicesEstablishment();
                dashBoardPage.clickaddAccountManager();
                addManager.enterServiceName(serviceid);
                //Add manager Page
                addManager.enterFacilityNum(facilitynum, facility_num2);
                addManager.enterFacilityIdNum(facility_id);
                addManager.clickSearchS2();
                addManager.clickOndate(calendertype, test);
                addManager.selectMonth(month, test);
                addManager.selectYear(year, test);
                addManager.selectDatelist(date);
                update.closeBtnDate();
                addManager.clickSaveBtn();
                addManager.enterOtpNum(optpNum);
                addManager.clickOtpBtn();


                //Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text);
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

            }



            catch (Exception e)
            {
                Console.WriteLine("hey catch here here");

                excelHelper.writedata(Filepath, "Service1", 17, 1, "Fail");

                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }
        }

        //Verify that user is unable to add the account manager with Past Expiry data 
        [Test, Order(3), Category("SmokeTest")]
        public void ACM_TC_06_ExpireDate()
        {

            // for (int i = 1; i <= RowCount; i++)
            //{
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");
            try
            {
                String user = excelHelper.ReadData(2, "Username");
                String pw = excelHelper.ReadData(2, "Password");
                String serviceid = excelHelper.ReadData(2, "Service Id");
                String unifiedid = excelHelper.ReadData(2, "Unified 1");
                String unifiedid2 = excelHelper.ReadData(2, "Unified 2");
                String fname = excelHelper.ReadData(2, "First Name");
                String lname = excelHelper.ReadData(2, "Last Name");
                String Nationality = excelHelper.ReadData(2, "Nationality");
                String calendertype = excelHelper.ReadData(2, "CalendarType");
                String year = excelHelper.ReadData(2, "Year");
                String month = excelHelper.ReadData(2, "Month");
                String date = excelHelper.ReadData(2, "Date");
                String id = excelHelper.ReadData(2, "New Unified ID");
                String Expected = excelHelper.ReadData(2, "Expected Result");


                //

                String facilitynum = excelHelper.ReadData(2, "Establishment 1");
                String facility_num2 = excelHelper.ReadData(2, "Establishment 2");
                String facility_id = excelHelper.ReadData(2, "New Unified ID");
                String optpNum = excelHelper.ReadData(2, "Captcha");




                loginmyclientpage.enterUsername(user);
                loginmyclientpage.enterPassword(pw);
                loginmyclientpage.clickSubmit(pw, test);
                //dashBoardPage.clickManagementeEstablishments1();
                dashBoardPage.clickServicesEstablishment();
                dashBoardPage.clickaddAccountManager();
                addManager.enterServiceName(serviceid);
                //Add manager Page
                addManager.enterFacilityNum(facilitynum, facility_num2);
                addManager.enterFacilityIdNum(facility_id);
                addManager.clickSearchS2();
                addManager.clickOndate(calendertype, test);
                //addManager.selectMonth(month, test);
                addManager.selectYear(year, test);
                addManager.selectDatelist(date);
                update.closeBtnDate();
                addManager.clickSaveBtn();
                //addManager.enterOtpNum(optpNum);
                //addManager.clickOtpBtn();

                Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_valCalRoleEndDate']")).Text);
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

            }



            catch (Exception e)
            {
                Console.WriteLine("hey catch here here");

                excelHelper.writedata(Filepath, "Service1", 17, 1, "Fail");

                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }
        }
        [Test, Order(3), Category("SmokeTest")]
        public void ACM_TC_07_InvalidID()
        {

            // for (int i = 1; i <= RowCount; i++)
            //{
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");
            try
            {
                String user = excelHelper.ReadData(2, "Username");
                String pw = excelHelper.ReadData(2, "Password");
                String serviceid = excelHelper.ReadData(2, "Service Id");
                String unifiedid = excelHelper.ReadData(2, "Unified 1");
                String unifiedid2 = excelHelper.ReadData(2, "Unified 2");
                String fname = excelHelper.ReadData(2, "First Name");
                String lname = excelHelper.ReadData(2, "Last Name");
                String Nationality = excelHelper.ReadData(2, "Nationality");
                String calendertype = excelHelper.ReadData(2, "CalendarType");
                String year = excelHelper.ReadData(2, "Year");
                String month = excelHelper.ReadData(2, "Month");
                String date = excelHelper.ReadData(2, "Date");
                String id = excelHelper.ReadData(2, "New Unified ID");
                String Expected = excelHelper.ReadData(2, "Expected Result");


                //

                String facilitynum = excelHelper.ReadData(2, "Establishment 1");
                String facility_num2 = excelHelper.ReadData(2, "Establishment 2");
                String facility_id = excelHelper.ReadData(2, "New Unified ID");
                String optpNum = excelHelper.ReadData(2, "Captcha");




                loginmyclientpage.enterUsername(user);
                loginmyclientpage.enterPassword(pw);
                loginmyclientpage.clickSubmit(pw, test);
                //dashBoardPage.clickManagementeEstablishments1();
                dashBoardPage.clickServicesEstablishment();
                dashBoardPage.clickaddAccountManager();
                addManager.enterServiceName(serviceid);
                //Add manager Page
                addManager.enterFacilityNum(facilitynum, facility_num2);
                addManager.enterFacilityIdNum(facility_id);
                addManager.clickSearchS2();
                Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_valCalRoleEndDate']")).Text);
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

                /*
                
                addManager.clickOndate(calendertype, test);
                //addManager.selectMonth(month, test);
                addManager.selectYear(year, test);
                addManager.selectDatelist(date);
                update.closeBtnDate();
                addManager.clickSaveBtn();
                //addManager.enterOtpNum(optpNum);
                //addManager.clickOtpBtn();
                */
            }



            catch (Exception e)
            {
                Console.WriteLine("hey catch here here");
                Console.WriteLine(e);
                excelHelper.writedata(Filepath, "Service1", 17, 1, "Fail");

                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }
        }





        [SetUp]
        public void BeforeTest()
        {
            test.AssignCategory("Smoke Test Suite of  " + MethodBase.GetCurrentMethod().DeclaringType.Name + " ");

        }
        [TearDown]
        public void clearresult()
        {
            excelHelper.datcollist.Clear();
            //driver.Quit();

        }
    }
}
