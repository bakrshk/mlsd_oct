﻿using AventStack.ExtentReports;
using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MlsdTest.TestCases
{
    [TestFixture]
    public class LoginMyClient_test : BaseClass
    {

        int RowCount;
        String Filepath = ExcelFilepath + "\\Data\\Registrationdata.xlsx";



        [SetUp]
        public void BeforeTest()
        {
            test.AssignCategory("Smoke Test Suite of  " + MethodBase.GetCurrentMethod().DeclaringType.Name + " ");

        }
        [TearDown]
        public void clearresult()
        {
            excelHelper.datcollist.Clear();

        }



        [Test, Order(1), Category("SmokeTest")]
        public void testing()
        {
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "LoginSheet");
            String currentURL = driver.Url;


            try
            {
                Console.WriteLine("hey try here");
                loginmyclientpage.Login_to_MyServices(excelHelper.ReadData(1, "Username"), excelHelper.ReadData(1, "Password"), test);
                Assert.AreEqual(excelHelper.ReadData(1, "Expected Result"), currentURL);
                excelHelper.writedata(Filepath, "LoginSheet", 6, 1, "Pass");
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Passed ");
                //homepage.click_Logout(test);   

            }

            catch (Exception e)
            {
                Console.WriteLine("hey catch here here");

                excelHelper.writedata(Filepath, "LoginSheet", 6, 1, "Fail");
                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }





        }

    }
    /*
              [Test, Order(2), Category("SmokeTest")]
        public void LoginClient()
        {
            String user = @"testad\testinguser1";
            String pw = "MolXyz789";

            loginmyclientpage.Login_to_MyServices(user, pw, test);



        }
     */
}

