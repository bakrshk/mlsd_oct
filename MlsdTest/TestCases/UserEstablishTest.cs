﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest.TestCases
{
    public class UserEstablishTest : BaseClass
    {
        int RowCount;
        String Filepath = ExcelFilepath + "\\Data\\Update.xlsx";
        String Servicepath = ExcelFilepath + "\\Data\\Services.xlsx";

        //Add  Account Service 3 against estblishemnt
        [Test, Order(1), Category("SmokeTest")]
        public void UserEstablish_Tc_08()

        {

            for (int i = 1; i <= RowCount; i++)
            {
                RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");
                try
                {
                    String user = excelHelper.ReadData(1, "Username");
                    String pw = excelHelper.ReadData(1, "Password");
                    String serviceid = excelHelper.ReadData(1, "Service Id");
                    String unifiedid = excelHelper.ReadData(1, "Unified 1");
                    String unifiedid2 = excelHelper.ReadData(1, "Unified 2");
                    String fname = excelHelper.ReadData(1, "First Name");
                    String lname = excelHelper.ReadData(1, "Last Name");
                    String Nationality = excelHelper.ReadData(1, "Nationality");
                    String calendertype = excelHelper.ReadData(1, "CalendarType");
                    String year = excelHelper.ReadData(1, "Year");
                    String month = excelHelper.ReadData(1, "Month");
                    String date = excelHelper.ReadData(1, "Date");
                    String id = excelHelper.ReadData(1, "New Unified ID");
                    String Expected = excelHelper.ReadData(1, "Expected Result");


                    //
                    String facilitynum = excelHelper.ReadData(1, "Establishment 1");
                    String facility_num2 = excelHelper.ReadData(1, "Establishment 2");
                    String facility_id = excelHelper.ReadData(1, "New Unified ID");
                    String optpNum = excelHelper.ReadData(1, "Captcha");
                    /*
                    String facilitynum = "9";
                    String facility_num2 = "1241";
                    String facility_id = "1002000618";
                    String optpNum = "796314";
                    */



                    loginmyclientpage.enterUsername(user);
                    loginmyclientpage.enterPassword(pw);
                    loginmyclientpage.clickSubmit(pw, test);
                    dashBoardPage.clickManagementeEstablishments1();
                    dashBoardPage.clickServicesEstablishment();
                    dashBoardPage.clickaddEstablishUser();
                    addManager.enterServiceName(serviceid);
                    //Add manager Page
                    addManager.enterFacilityNum(facilitynum, facility_num2);
                    addManager.enterFacilityIdNum(facility_id);
                    addManager.clickSearchS2();
                    addManager.clickOndate(calendertype, test);
                    addManager.selectMonth(month, test);
                    addManager.selectYear(year, test);
                    addManager.selectDatelist(date);
                    //update.closeBtnDate();
                    addEstablish.checkAllService();
                    addManager.clickSaveBtn();



                    //Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text);
                    test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

                }



                catch (Exception e)
                {
                    Console.WriteLine("hey catch here here");

                    excelHelper.writedata(Filepath, "Service1", 17, 1, "Fail");

                    test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                    test.Info(e);
                    test.AddScreenCaptureFromPath(Capture(driver));
                }
            }
        }

        //Verify that system can add User after marking all the available services checkbox
        [Test, Order(3), Category("SmokeTest")]
        public void UserEstablish_Tc_09()
        {

            // for (int i = 1; i <= RowCount; i++)
            //{
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");
            try
            {
                String user = excelHelper.ReadData(1, "Username");
                String pw = excelHelper.ReadData(1, "Password");
                String serviceid = excelHelper.ReadData(1, "Service Id");
                String unifiedid = excelHelper.ReadData(1, "Unified 1");
                String unifiedid2 = excelHelper.ReadData(1, "Unified 2");
                String fname = excelHelper.ReadData(1, "First Name");
                String lname = excelHelper.ReadData(1, "Last Name");
                String Nationality = excelHelper.ReadData(1, "Nationality");
                String calendertype = excelHelper.ReadData(1, "CalendarType");
                String year = excelHelper.ReadData(1, "Year");
                String month = excelHelper.ReadData(1, "Month");
                String date = excelHelper.ReadData(1, "Date");
                String id = excelHelper.ReadData(1, "New Unified ID");
                String Expected = excelHelper.ReadData(1, "Expected Result");


                //
                String facilitynum = excelHelper.ReadData(1, "Establishment 1");
                String facility_num2 = excelHelper.ReadData(1, "Establishment 2");
                String facility_id = excelHelper.ReadData(1, "New Unified ID");
                String optpNum = excelHelper.ReadData(1, "Captcha");
                /*
                String facilitynum = "9";
                String facility_num2 = "1241";
                String facility_id = "1002000618";
                String optpNum = "796314";
                */



                loginmyclientpage.enterUsername(user);
                loginmyclientpage.enterPassword(pw);
                loginmyclientpage.clickSubmit(pw, test);
                //dashBoardPage.clickManagementeEstablishments1();
                dashBoardPage.clickServicesEstablishment();
                dashBoardPage.clickaddEstablishUser();
                addManager.enterServiceName(serviceid);
                //Add manager Page
                addManager.enterFacilityNum(facilitynum, facility_num2);
                addManager.enterFacilityIdNum(facility_id);
                addManager.clickSearchS2();
                addManager.clickOndate(calendertype, test);
                addManager.selectMonth(month, test);
                addManager.selectYear(year, test);
                addManager.selectDatelist(date);
                //update.closeBtnDate();
                addEstablish.checkAllService();
                addManager.clickSaveBtn();
                addManager.enterOtpNum(optpNum);
                addManager.clickOtpBtn();


                //Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text);
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

            }



            catch (Exception e)
            {
                Console.WriteLine("hey catch here here");

                excelHelper.writedata(Filepath, "Service1", 17, 1, "Fail");

                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }
        }

        //Verify that User can add built in user after marking random available services checkboxes
        [Test, Order(3), Category("SmokeTest")]
        public void UserEstablish_Tc_10_RandomService()
        {

            // for (int i = 1; i <= RowCount; i++)
            //{
            RowCount = excelHelper.PopulateInCollecetion(Servicepath, "Service2");
            try
            {
                String user = excelHelper.ReadData(1, "Username");
                String pw = excelHelper.ReadData(1, "Password");
                String serviceid = excelHelper.ReadData(1, "Service Id");
                String unifiedid = excelHelper.ReadData(1, "Unified 1");
                String unifiedid2 = excelHelper.ReadData(1, "Unified 2");
                String fname = excelHelper.ReadData(1, "First Name");
                String lname = excelHelper.ReadData(1, "Last Name");
                String Nationality = excelHelper.ReadData(1, "Nationality");
                String calendertype = excelHelper.ReadData(1, "CalendarType");
                String year = excelHelper.ReadData(1, "Year");
                String month = excelHelper.ReadData(1, "Month");
                String date = excelHelper.ReadData(1, "Date");
                String id = excelHelper.ReadData(1, "New Unified ID");
                String Expected = excelHelper.ReadData(1, "Expected Result");


                //
                String facilitynum = excelHelper.ReadData(1, "Establishment 1");
                String facility_num2 = excelHelper.ReadData(1, "Establishment 2");
                String facility_id = excelHelper.ReadData(1, "SuperId");
//Methods
                loginmyclientpage.enterUsername(user);
                loginmyclientpage.enterPassword(pw);
                loginmyclientpage.clickSubmit(pw, test);
                //dashBoardPage.clickManagementeEstablishments1();
                dashBoardPage.clickServicesEstablishment();
                dashBoardPage.clickaddEstablishUser();
                addManager.enterServiceName(serviceid);
                //Add manager Page
                addManager.enterFacilityNum(facilitynum, facility_num2);
                addManager.enterFacilityIdNum(facility_id);
                addManager.clickSearchS2();
                addManager.clickOndate(calendertype, test);
                addManager.selectMonth(month, test);
                addManager.selectYear(year, test);
                addManager.selectDatelist(date);
                //update.closeBtnDate();
                addEstablish.selectRandomService();
                addManager.clickSaveBtn();


                //Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text);
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

            }
            catch (Exception e)
            {
                Console.WriteLine("hey catch here here");

                excelHelper.writedata(Filepath, "Service1", 17, 1, "Fail");

                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }
        }
        //Verify that user can add built in user without marking any available services checkboxes
        [Test, Order(3), Category("SmokeTest")]
        public void UserEstablish_Tc_11_NoService()
        {

            // for (int i = 1; i <= RowCount; i++)
            //{
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");
            try
            {
                String user = excelHelper.ReadData(4, "Username");
                String pw = excelHelper.ReadData(4, "Password");
                String serviceid = excelHelper.ReadData(4, "Service Id");
                String unifiedid = excelHelper.ReadData(4, "Unified 1");
                String unifiedid2 = excelHelper.ReadData(4, "Unified 2");
                String fname = excelHelper.ReadData(4, "First Name");
                String lname = excelHelper.ReadData(4, "Last Name");
                String Nationality = excelHelper.ReadData(4, "Nationality");
                String calendertype = excelHelper.ReadData(4, "CalendarType");
                String year = excelHelper.ReadData(4, "Year");
                String month = excelHelper.ReadData(4, "Month");
                String date = excelHelper.ReadData(4, "Date");
                String id = excelHelper.ReadData(4, "New Unified ID");
                String Expected = excelHelper.ReadData(4, "Expected Result");


                //
                String facilitynum = excelHelper.ReadData(4, "Establishment 1");
                String facility_num2 = excelHelper.ReadData(4, "Establishment 2");
                String facility_id = excelHelper.ReadData(4, "New Unified ID");
                String optpNum = excelHelper.ReadData(4, "Captcha");
                //Methods
                loginmyclientpage.enterUsername(user);
                loginmyclientpage.enterPassword(pw);
                loginmyclientpage.clickSubmit(pw, test);
                //dashBoardPage.clickManagementeEstablishments1();
                dashBoardPage.clickServicesEstablishment();
                dashBoardPage.clickaddEstablishUser();
                addManager.enterServiceName(serviceid);
                //Add manager Page
                addManager.enterFacilityNum(facilitynum, facility_num2);
                addManager.enterFacilityIdNum(facility_id);
                addManager.clickSearchS2();
                addManager.clickOndate(calendertype, test);
                addManager.selectMonth(month, test);
                addManager.selectYear(year, test);
                addManager.selectDatelist(date);
                //update.closeBtnDate();
                addManager.clickSaveBtn();



                Assert.AreEqual(Expected, driver.FindElement(By.XPath("//div[@id='MainContent_divError']")).Text);
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

                //excelHelper.writedata(Filepath, "SignUp", 16, i + 1, "Fail");

            }



            catch (Exception e)
            {
                Console.WriteLine("hey catch here here");

                excelHelper.writedata(Filepath, "Service1", 17, 1, "Fail");

                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }
        }


        //Verify that user can add built in user without marking any available services checkboxes
        [Test, Order(3), Category("SmokeTest")]
        public void UserEstablish_Tc_12_InvalidID()
        {

            // for (int i = 1; i <= RowCount; i++)
            //{
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");
            try
            {
                String user = excelHelper.ReadData(5, "Username");
                String pw = excelHelper.ReadData(5, "Password");
                String serviceid = excelHelper.ReadData(5, "Service Id");
                String unifiedid = excelHelper.ReadData(5, "Unified 1");
                String unifiedid2 = excelHelper.ReadData(5, "Unified 2");
                String fname = excelHelper.ReadData(5, "First Name");
                String lname = excelHelper.ReadData(5, "Last Name");
                String Nationality = excelHelper.ReadData(5, "Nationality");
                String calendertype = excelHelper.ReadData(5, "CalendarType");
                String year = excelHelper.ReadData(5, "Year");
                String month = excelHelper.ReadData(5, "Month");
                String date = excelHelper.ReadData(5, "Date");
                String id = excelHelper.ReadData(5, "New Unified ID");
                String Expected = excelHelper.ReadData(5, "Expected Result");


                //
                String facilitynum = excelHelper.ReadData(5, "Establishment 1");
                String facility_num2 = excelHelper.ReadData(5, "Establishment 2");
                String facility_id = excelHelper.ReadData(5, "New Unified ID");
                String optpNum = excelHelper.ReadData(5, "Captcha");
                //Methods
                loginmyclientpage.enterUsername(user);
                loginmyclientpage.enterPassword(pw);
                loginmyclientpage.clickSubmit(pw, test);
                //dashBoardPage.clickManagementeEstablishments1();
                dashBoardPage.clickServicesEstablishment();
                dashBoardPage.clickaddEstablishUser();
                addManager.enterServiceName(serviceid);
                //Add manager Page
                addManager.enterFacilityNum(facilitynum, facility_num2);
                addManager.enterFacilityIdNum(facility_id);
                //addManager.clickSearchS2();
                Thread.Sleep(3000);

                //Assert.IsTrue(driver.FindElement(By.XPath("//span[@id='MainContent_cValIDNumber']")).Text.Contains(Expected));
                Console.WriteLine();
                Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_cValIDNumber']")).Text);
                //excelHelper.writedata
                excelHelper.writedata(Filepath, "Service2", 17, 6, "Pass");
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

            }
            catch (Exception e)
            {
                Console.WriteLine("hey catch here here" + e.Message);
                Assert.Fail();
                excelHelper.writedata(Filepath, "Service1", 17, 7, "Fail");

                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }
        }


        /**************************************test*************************/
        [Test, Order(3), Category("SmokeTest")]
        public void checkingPractice()
        {

            // for (int i = 1; i <= RowCount; i++)
            //{
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");
            try
            {
                String user = excelHelper.ReadData(1, "Username");
                String pw = excelHelper.ReadData(1, "Password");
                String serviceid = excelHelper.ReadData(1, "Service Id");
                String unifiedid = excelHelper.ReadData(1, "Unified 1");
                String unifiedid2 = excelHelper.ReadData(1, "Unified 2");
                String fname = excelHelper.ReadData(1, "First Name");
                String lname = excelHelper.ReadData(1, "Last Name");
                String Nationality = excelHelper.ReadData(1, "Nationality");
                String calendertype = excelHelper.ReadData(1, "CalendarType");
                String year = excelHelper.ReadData(1, "Year");
                String month = excelHelper.ReadData(1, "Month");
                String date = excelHelper.ReadData(1, "Date");
                String id = excelHelper.ReadData(1, "New Unified ID");
                String Expected = excelHelper.ReadData(1, "Expected Result");


                //
                String facilitynum = excelHelper.ReadData(1, "Establishment 1");
                String facility_num2 = excelHelper.ReadData(1, "Establishment 2");
                String facility_id = excelHelper.ReadData(1, "New Unified ID");
                String optpNum = excelHelper.ReadData(1, "Captcha");
                //Methods
                loginmyclientpage.enterUsername(user);
                loginmyclientpage.enterPassword(pw);
                loginmyclientpage.clickSubmit(pw, test);
                //dashBoardPage.clickManagementeEstablishments1();
                dashBoardPage.clickServicesEstablishment();
                dashBoardPage.clickaddEstablishUser();
                addManager.enterServiceName(serviceid);
                //Add manager Page
                addManager.enterFacilityNum(facilitynum, facility_num2);
                addManager.enterFacilityIdNum(facility_id);
                addManager.clickSearchS2();
                addEstablish.clickOndate(calendertype, test);
                addManager.selectMonth(month, test);
                addManager.selectYear(year, test);
                addEstablish.selectDatelist(date);
                //update.closeBtnDate();
                addEstablish.selectRandomService();
                addManager.clickSaveBtn();
                addManager.enterOtpNum(optpNum);
                addManager.clickOtpBtn();


                //Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text);
                test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

            }
            catch (Exception e)
            {
                Console.WriteLine("hey catch here here");

                Console.WriteLine(e.Message);

                excelHelper.writedata(Filepath, "Service1", 17, 1, "Fail");

                test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                test.Info(e);
                test.AddScreenCaptureFromPath(Capture(driver));
            }
        }





        [SetUp]
        public void BeforeTest()
        {
            test.AssignCategory("Smoke Test Suite of  " + MethodBase.GetCurrentMethod().DeclaringType.Name + " ");

        }
        [TearDown]
        public void clearresult()
        {
            excelHelper.datcollist.Clear();
            driver.Quit();

        }
    }
}
