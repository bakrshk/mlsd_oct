﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest.TestCases
{
 [TestFixture]
    public class ViewNewFileDataTest : BaseClass
    {

        int RowCount;
        String Filepath = ExcelFilepath + "\\Data\\Services.xlsx";
        String Registrationpath = ExcelFilepath + "\\Data\\Registrationdata.xlsx";

        public object SeleniumExtras { get; private set; }

        [Test, Order(1), Category("SmokeTest")]

        public void FileDataAcceptingRequest()
        {
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "ViewNewFileData");

            for (int i = 0; i < RowCount; i++)
            {
                String EstablishhmentNumber = excelHelper.ReadData(i, "Establishment Number");
                String Reason = excelHelper.ReadData(i, "Reason");
                String Decision = excelHelper.ReadData(i, "Expected Result");
                String Actual = excelHelper.ReadData(i, "Actual Result");

                String Result = excelHelper.ReadData(i, "Status");
                String Action = excelHelper.ReadData(i, "Action");
                try
                {
                    dashBoardPage.FacilityManagementRepresentative(test);
                     dashBoardPage.ClickManagerEnterprise(test);
                    dashBoardPage.ClickViewNewFile(test);
                    viewNewFileDataPage.ClickReview(EstablishhmentNumber, test);
                   // viewNewFileDataPage.AlertAccept(test);
                    viewNewFileDataPage.ClickTabTypeBusinessActivity(test);
                    viewNewFileDataPage.ClickBtnNextOne(test);
                    viewNewFileDataPage.ClickTabUnifiedNumber(test);
                    viewNewFileDataPage.ClickBtnNextOne(test);

                    viewNewFileDataPage.ClickTabLicensingData(test);
                    viewNewFileDataPage.ClickBtnNextOne(test);
                    viewNewFileDataPage.ClickTabTypeBusinessActivity(test);
                    viewNewFileDataPage.ClickBtnNextOne(test);
                    viewNewFileDataPage.ClickTabTitle(test);

                    switch (Action)
                    {
                        case "Accept":
                            viewNewFileDataPage.ClickBtnConsent(test);
                            viewNewFileDataPage.AlertCancel(test);

                            break;

                        case "Reject":
                            viewNewFileDataPage.ClickBtnReject(test);
                            viewNewFileDataPage.EnterRejectDescription(Reason,test);
                            viewNewFileDataPage.ClickBtnRejectApplication(test);
                            viewNewFileDataPage.AlertCancel(test);

                            break;

                        case "Cancel":
                            viewNewFileDataPage.ClickBtnCancel(test);
                            viewNewFileDataPage.AlertCancel(test);
                            break;

                            //viewNewFileDataPage.AlertAccept(test);
                            viewNewFileDataPage.AlertCancel(test);

                    }

                    excelHelper.writedata(Filepath, "ViewNewFileData", 6, i + 1, "Pass");

                    test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + "Pass");
                }
                catch (Exception e)
                {
                    excelHelper.writedata(Filepath, "ViewNewFileData", 6, i + 1, "fail");
                    excelHelper.writedata(Filepath, "ViewNewFileData", 7, i + 1, e.Message);

                    test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                    test.Info(e);
                    test.AddScreenCaptureFromPath(Capture(driver));
                }
            }
        }

        [SetUp]
        public void BeforeTest()
        {
            int LogCount;

            LogCount = excelHelper.PopulateInCollecetion(Registrationpath, "LoginSheet");
            String user = excelHelper.ReadData(1, "Username");
            String pw = excelHelper.ReadData(1, "Password");
            loginmyclientpage.enterUsername(user);
            loginmyclientpage.enterPassword(pw);
            loginmyclientpage.clickSubmit(pw, test);
            test.AssignCategory("Smoke Test Suite of  " + MethodBase.GetCurrentMethod().DeclaringType.Name + "");
        }

        [TearDown]
        public void clearresult()
        {
            excelHelper.datcollist.Clear();
            driver.Quit();
            

        }
    }
}