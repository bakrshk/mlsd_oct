﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest.TestCases
{
    [TestFixture]
    public class UnifiedAccountManagerChange : BaseClass
    {

        int RowCount;
        String Filepath = ExcelFilepath + "\\Data\\Services.xlsx";

        public object SeleniumExtras { get; private set; }

        [Test, Order(1), Category("SmokeTest")]


        public void ChangeAccountTest()
        {
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service1");

            for (int i = 2; i <= RowCount; i++)
            {
                try
                {
                   
                    String serviceid = excelHelper.ReadData(i, "Service Id");
                    String unifiedid = excelHelper.ReadData(i, "Unified 1");
                    String unifiedid2 = excelHelper.ReadData(i, "Unified 2");
                    String fname = excelHelper.ReadData(i, "First Name");
                    String lname = excelHelper.ReadData(i, "Last Name");
                    String Nationality = excelHelper.ReadData(i, "Nationality");
                    String calendertype = excelHelper.ReadData(i, "CalendarType");
                    String year = excelHelper.ReadData(i, "Year");
                    String month = excelHelper.ReadData(i, "Month");
                    String date = excelHelper.ReadData(i, "Date");
                    String id = excelHelper.ReadData(i, "New Unified ID");
                    String description = excelHelper.ReadData(i, "Description");
                    String mobile = excelHelper.ReadData(i, "Mobile");

                    String Expected = excelHelper.ReadData(i, "Expected Result");


                    //dashBoardPage.clickManagementeEstablishments1();
                    driver.Navigate().GoToUrl("http://mol-stage-app3.molstagedom.net:9100/MyClients/Pages/default.aspx");
                    dashBoardPage.clickManagementeEstablishments1();

                    dashBoardPage.clickServicesEstablishment();
                    dashBoardPage.clickChangeUnified();
                    update.enterStudentServiceID(serviceid);
                    update.enterUnifiedID(unifiedid, unifiedid2);
                    update.clickSearchBtn();
                    update.enterNewUnifiedID(id);
                    update.enterFirstName(fname);
                    update.enterLastname(lname);
                    update.clickOndate(calendertype,test);
                    update.SelectYearMonth(year, month, test);
                    update.selectDatelist(date);
                    //update.clickSearchBtn();
                    update.selectNationailty(Nationality, test);
                    update.clickOnSearchData();
                    update.EnterDescription(description);
                    update.EnterMobileNumber("0511111111");
                    update.ClickSubmit(test);
                    //update.verify();
                    new WebDriverWait(driver, TimeSpan.FromSeconds(25)).Until(ExpectedConditions.ElementIsVisible(By.Id("MainContent_lblConfirm")));

                    var Pass = driver.FindElement(By.Id("MainContent_lblConfirm")).Text;

                    // new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.AlertIsPresent());
                    Assert.AreEqual(Expected, driver.FindElement(By.Id("MainContent_lblConfirm")).Text);
                    excelHelper.writedata(Filepath, "Service1", 16, i+1, "Pass");
                    excelHelper.writedata(Filepath, "Service1", 17, i+1, Pass);

                    test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");

                }
                catch (Exception e)
                {
                    Console.WriteLine("hey catch here here");
                    var error = driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text;
                    excelHelper.writedata(Filepath, "Service1", 16, i+1, "fail");
                    excelHelper.writedata(Filepath, "Service1", 17, i + 1, error);

                    Assert.Fail(e.Message);

                    test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                    test.Info(e);
                    test.AddScreenCaptureFromPath(Capture(driver));
                }
            }
        }
  

        [SetUp]
        public void BeforeTest()
        {
            loginmyclientpage.enterUsername(@"testad\testinguser10");
            loginmyclientpage.enterPassword("MolXyz789");
            loginmyclientpage.clickSubmit("MolXyz789", test);
            test.AssignCategory("Smoke Test Suite of  " + MethodBase.GetCurrentMethod().DeclaringType.Name + " ");

        }
        [TearDown]
        public void clearresult()
        {
            excelHelper.datcollist.Clear();
            driver.Quit();

        }
    }
}
