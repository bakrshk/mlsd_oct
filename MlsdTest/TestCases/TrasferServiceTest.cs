﻿
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest.TestCases
{
  [TestFixture]
    public class TrasferServiceTest : BaseClass
    {

        String Filepath = ExcelFilepath + "\\Data\\Transfer.xlsx";
        String Registrationpath = ExcelFilepath + "\\Data\\Registrationdata.xlsx";
        int RowCounter;

        public object SeleniumExtras { get; private set; }

        [Test, Order(1), Category("SmokeTest")]

        public void TransferLabourTest()
        {
            RowCounter = excelHelper.PopulateInCollecetion(Filepath, "Transfer Serivce");

            Console.WriteLine(RowCounter);
            for (int i = 1; i <= RowCounter; i++)
            {
                String UnifiedNum = excelHelper.ReadData(i, "Unified Office");
                String SequenceNum = excelHelper.ReadData(i, "Sequence");
                String ServiceID = excelHelper.ReadData(i, "Service Id");
                //String UnifiedNum = "1";
                //String SequenceNum = "116410";
                //String ServiceID = "1039786676";
                String LabourID = excelHelper.ReadData(i, "Labour Id");
                String Expected = excelHelper.ReadData(i, "Expected Result");

                String Actual = "";

                try
                {
                    dashBoardPage.ClickTransferservices(test);
                    dashBoardPage.ClickInComingAgentService(test);
                    dashBoardPage.ClickSubmitRequestTransfer(test);
                    transferServicePage.EnterUnifiedOffice(UnifiedNum,test);
                    transferServicePage.EnterSequence(SequenceNum, test);
                    transferServicePage.EnterServiceNum(ServiceID, test);
                    transferServicePage.ClickBtnCheck(test);
                    transferServicePage.EnterWorkerResidenceNum(LabourID, test);
                    transferServicePage.ClickBtnSearch(test);
                    transferServicePage.ClickBtnPrint(test) ;
                    transferServicePage.ClickCheckedApproved(test);
                    transferServicePage.ClickBtnSubmit(test);
                    transferServicePage.getSuccessfullMessage(Actual, test);


                    //var Pass = driver.FindElement(By.Id("MainContent_lblConfirm")).Text;
                    excelHelper.writedata(Filepath, "TransferService", 6, i + 1, "Pass");

                    excelHelper.writedata(Filepath, "TransferService", 5, i + 1, Actual);

                    test.Log(Status.Pass, TestContext.CurrentContext.Test.Name +  Actual);

                }
                    catch (Exception e)
                {

                    var error = driver.FindElement(By.Id("MainContent_SponsorTransfer1_divError"));
                    excelHelper.writedata(Filepath, "Transfer Serivce", 6, i + 1, "fail");
                    excelHelper.writedata(Filepath, "Transfer Serivce", 5, i + 1, error.Text);

                    //Assert.Fail(e.Message);

                    test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                    test.Info(e);
                    //test.AddScreenCaptureFromPath(Capture(driver));
                    
                    continue;
            




                }
            }
        }


        [SetUp]
        public void BeforeTest()
        {
            int LogCount;

            LogCount = excelHelper.PopulateInCollecetion(Registrationpath, "LoginSheet");
            String user = excelHelper.ReadData(1, "Username");
            String pw = excelHelper.ReadData(1, "Password");
            loginmyclientpage.enterUsername(user);
            loginmyclientpage.enterPassword(pw);
            loginmyclientpage.clickSubmit(pw, test);
            test.AssignCategory("Smoke Test Suite of  " + MethodBase.GetCurrentMethod().DeclaringType.Name + " ");

        }
        [TearDown]
        public void clearresult()
        {
            excelHelper.datcollist.Clear();
            driver.Quit();

        }
    }
}
