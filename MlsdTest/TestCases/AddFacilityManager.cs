﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest.TestCases

{
    [TestFixture]
    public class AddFacilityManager : BaseClass
    {

        int RowCount;
        String Filepath = ExcelFilepath + "\\Data\\Services.xlsx";

        public object SeleniumExtras { get; private set; }

        [Test, Order(2), Category("SmokeTest")]
        public void AddAccountTest()
        {
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service2");

            for (int i = 1; i <= RowCount; i++)
            {
                try
                {
                    String user = excelHelper.ReadData(i, "Username");
                    String pw = excelHelper.ReadData(i, "Password");
                    String serviceid = excelHelper.ReadData(i, "Service Id");
                    String unifiedid = excelHelper.ReadData(i, "Unified 1");
                    String unifiedid2 = excelHelper.ReadData(i, "Unified 2");
                    String fname = excelHelper.ReadData(i, "First Name");
                    String lname = excelHelper.ReadData(i, "Last Name");
                    String Nationality = excelHelper.ReadData(i, "Nationality");
                    String calendertype = excelHelper.ReadData(i, "CalendarType");
                    String year = excelHelper.ReadData(i, "Year");
                    String month = excelHelper.ReadData(i, "Month");
                    String date = excelHelper.ReadData(i, "Date");
                    String id = excelHelper.ReadData(i, "New Unified ID");
                    String Expected = excelHelper.ReadData(i, "Expected Result");


                    //
                    String facility_id = excelHelper.ReadData(i, "Super_id");




                    loginmyclientpage.enterUsername(user);
                    loginmyclientpage.enterPassword(pw);
                    loginmyclientpage.clickSubmit(pw, test);
                    // driver.Navigate().GoToUrl("http://mol-stage-app3.molstagedom.net:9100/MyClients/Pages/default.aspx");

                    dashBoardPage.clickManagementeEstablishments1();
                    dashBoardPage.clickServicesEstablishment();
                    dashBoardPage.clickaddAccountManager();
                    addManager.enterServiceName(serviceid);
                    //Add manager Page
                    addManager.enterFacilityNum(unifiedid, unifiedid2);
                    addManager.enterFacilityIdNum(facility_id);
                    addManager.clickSearchS2();
                    addManager.clickOndate(calendertype, test);
                    addManager.selectMonth(month, test);
                    addManager.selectYear(year, test);
                    addManager.selectDatelist(date);
                    //update.closeBtnDate();
                    //Assert.Pass();
                    //addManager.clickSaveBtn();
                    test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");
                    excelHelper.writedata(Filepath, "Service2", 13, i, "Pass");

                }
                catch (Exception e)
                {
                    //var error = driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text;
                    //excelHelper.writedata(Filepath, "Service1", 18, 1, error);

                    Assert.Fail(e.Message);
                    excelHelper.writedata(Filepath, "Service2", 13, i, "Fail");

                    test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                    test.Info(e);
                    test.AddScreenCaptureFromPath(Capture(driver));
                }
            }
        }
        [Test, Order(3), Category("SmokeTest")]
        public void AddAccountTest2()
        {
            RowCount = excelHelper.PopulateInCollecetion(Filepath, "Service3");

            for (int i = 1; i <= RowCount; i++)
            {
                try
                {
                    String user = excelHelper.ReadData(i, "Username");
                    String pw = excelHelper.ReadData(i, "Password");
                    String serviceid = excelHelper.ReadData(i, "Service Id");
                    String unifiedid = excelHelper.ReadData(i, "Unified 1");
                    String unifiedid2 = excelHelper.ReadData(i, "Unified 2");
                    String fname = excelHelper.ReadData(i, "First Name");
                    String lname = excelHelper.ReadData(i, "Last Name");
                    String Nationality = excelHelper.ReadData(i, "Nationality");
                    String calendertype = excelHelper.ReadData(i, "CalendarType");
                    String year = excelHelper.ReadData(i, "Year");
                    String month = excelHelper.ReadData(i, "Month");
                    String date = excelHelper.ReadData(i, "Date");
                    String id = excelHelper.ReadData(i, "New Unified ID");
                    String Expected = excelHelper.ReadData(i, "Expected Result");


                    //
                    String facility_id = excelHelper.ReadData(i, "Super_id");


                    //

                    //Methods
                    loginmyclientpage.enterUsername(user);
                    loginmyclientpage.enterPassword(pw);
                    loginmyclientpage.clickSubmit(pw, test);
                    //dashBoardPage.clickManagementeEstablishments1();
                    dashBoardPage.clickServicesEstablishment();
                    dashBoardPage.clickaddEstablishUser();
                    addManager.enterServiceName(serviceid);
                    //Add manager Page
                    addManager.enterFacilityNum(unifiedid, unifiedid2);
                    addManager.enterFacilityIdNum(facility_id);
                    addManager.clickSearchS2();
                    addManager.clickOndate(calendertype, test);
                    addManager.selectMonth(month, test);
                    addManager.selectYear(year, test);
                    addManager.selectDatelist(date);
                    //update.closeBtnDate();
                    addEstablish.selectRandomService();
                    addManager.clickSaveBtn();


                    //Assert.AreEqual(Expected, driver.FindElement(By.XPath("//span[@id='MainContent_lblError']")).Text);
                    test.Log(Status.Pass, TestContext.CurrentContext.Test.Name + " Pass ");
                    excelHelper.writedata(Filepath, "Service3", 13, i, "Pass");


                }
                catch (Exception e)
                {
                    Console.WriteLine("hey catch here here");

                    excelHelper.writedata(Filepath, "Service3", 13, i, "Fail");

                    test.Log(Status.Fail, TestContext.CurrentContext.Test.Name + " Failed ");
                    test.Info(e);
                    test.AddScreenCaptureFromPath(Capture(driver));
                }
            }
            /*
            [SetUp]
            public void BeforeTest()
            {
                loginmyclientpage.enterUsername(@"testad\testinguser10");
                loginmyclientpage.enterPassword("MolXyz789");
                loginmyclientpage.clickSubmit("MolXyz789", test);
                test.AssignCategory("Smoke Test Suite of  " + MethodBase.GetCurrentMethod().DeclaringType.Name + " ");

            }
            [TearDown]
            public void clearresult()
            {
                excelHelper.datcollist.Clear();
               // driver.Quit();

            }
        */
        }

    }
}

    
    

