﻿
using AventStack.ExtentReports;
using MlsdTest.Base;
using MlsdTest.Helper;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoIt;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace MlsdTest.Pages
{
    public class TransferServicePage : BasePage
    {
        Actions actions;
        private IWebDriver driver;


        public object SeleniumExtras { get; private set; }

        public TransferServicePage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }


        //------------------------Transfer Labour to Other Company Elements------------------


        [FindsBy(How = How.Name, Using = "ctl00$MainContent$SponsorTransfer1$ucValidateBusinessUser$txtEstOfficeId")]
        IWebElement unifiedOffice { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$SponsorTransfer1$ucValidateBusinessUser$txtEstSequenceNo")]
        IWebElement unified2 { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$SponsorTransfer1$ucValidateBusinessUser$txtReuesterIdNo")]
        IWebElement serviceNum { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$SponsorTransfer1$txtLaborerIdNo")]
        IWebElement workerResidenceNum { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$SponsorTransfer1$ucValidateBusinessUser$btnValidate")]
        IWebElement btnCheck { get; set; }


        [FindsBy(How = How.Name, Using = "ctl00$MainContent$SponsorTransfer1$btnCheck")]
        IWebElement btnSearch { get; set; }



        [FindsBy(How = How.XPath, Using = "//div[@id='MainContent_SponsorTransfer1_divDisclaimer']//label")]
        IWebElement checkboxApproved { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_EstablishmentEdit1_btnReject")]
        IWebElement btnRejectApplication { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_SponsorTransfer1_btnSubmit")]
        IWebElement btnSubmit { get; set; }


        [FindsBy(How = How.Id, Using = "MainContent_SponsorTransfer1_lblSuccessfulMessage")]
        IWebElement SuccessMessage { get; set; }
       


        public void EnterUnifiedOffice(String element, ExtentTest test)
        {
            HelperMethod.ExplicitWaitByName(driver, "ctl00$MainContent$SponsorTransfer1$ucValidateBusinessUser$txtEstOfficeId");
            HelperMethod.EnterValue(unifiedOffice, element, "EnterUnifiedOffice");
            test.Log(Status.Info, "EnterUnifiedOffice");

        }


        public void EnterSequence(String element, ExtentTest test)
        {
            HelperMethod.ExplicitWaitByName(driver, "ctl00$MainContent$SponsorTransfer1$ucValidateBusinessUser$txtEstSequenceNo");
            HelperMethod.EnterValue(unified2, element, "EnterSequence");
            test.Log(Status.Info, "EnterUnifiedOffice");

        }
        public void EnterServiceNum(String element, ExtentTest test)
        {
            HelperMethod.ExplicitWaitByName(driver, "ctl00$MainContent$SponsorTransfer1$ucValidateBusinessUser$txtReuesterIdNo");
            HelperMethod.EnterValue(serviceNum, element, "EnterWorkerResidenceNum");
            test.Log(Status.Info, "EnterWorkerResidenceNum");

        }
        public void ClickBtnCheck(ExtentTest test)
        {

            HelperMethod.ExplicitWaitByName(driver, "ctl00$MainContent$SponsorTransfer1$ucValidateBusinessUser$btnValidate");

            HelperMethod.ClickOnIt(btnCheck, "Clicked on ClickBtnCheck");
            test.Log(Status.Info, "ClickBtnCheck Clicked");

        }
        public void EnterWorkerResidenceNum(String element, ExtentTest test)
        {
            HelperMethod.ExplicitWaitByName(driver, "ctl00$MainContent$SponsorTransfer1$txtLaborerIdNo");
            HelperMethod.EnterValue(workerResidenceNum, element, "EnterWorkerResidenceNum");
            test.Log(Status.Info, "EnterWorkerResidenceNum");

        }

        public void ClickBtnSearch(ExtentTest test)
        {

            HelperMethod.ExplicitWaitByName(driver, "ctl00$MainContent$SponsorTransfer1$btnCheck");

            HelperMethod.ClickOnIt(btnSearch, "Clicked on ClickBtnCheck");
            test.Log(Status.Info, "ClickBtnCheck Clicked");

        }
        public void ClickBtnPrint(ExtentTest test)
        {
            try
            {

                if (HelperMethod.IsElementPresent(By.Name("ctl00$MainContent$SponsorTransfer1$btnDisclaimerPrint")))
                {


                    ((IJavaScriptExecutor)driver).ExecuteScript("javascript:__doPostBack('ctl00$MainContent$SponsorTransfer1$btnDisclaimerPrint','')");
                }

            }

            catch(Exception e)
            {

            }
        
        

        }
        public void ClickCheckedApproved(ExtentTest test)
        {

            HelperMethod.ExplicitWaitByXpath(driver, "//div[@id='MainContent_SponsorTransfer1_divDisclaimer']//label");

            HelperMethod.ClickOnIt(checkboxApproved, "checkboxApproved on checkboxApproved");
            test.Log(Status.Info, "checkboxApproved Clicked");

        }

        public void ClickBtnSubmit(ExtentTest test)
        {

            HelperMethod.ExplicitWaitById(driver, "MainContent_SponsorTransfer1_btnSubmit");

            HelperMethod.ClickOnIt(btnSubmit, "Btn Submit");
            test.Log(Status.Info, "checkboxApproved Clicked");

        }
        public string getSuccessfullMessage(String Assert , ExtentTest test)
        {

            HelperMethod.ExplicitWaitById(driver, " MainContent_SponsorTransfer1_lblSuccessfulMessage");
            
            String Text = SuccessMessage.Text;
            test.Log(Status.Info, Assert);
            
            int start = 0;
            int end = 59;
            if (end < 0)
            {
                end = Text.Length + end;
            }
            int len = end - start;
            Assert = Text.Substring(start, len);
            return Assert;
        
        }
        public string getErrorMessage(String Error, ExtentTest test)
        {
            try
            {
                HelperMethod.ExplicitWaitById(driver, "MainContent_SponsorTransfer1_divError");
                return Error;

            }

            catch (Exception){
                return Error;
            }

        }


    }


    }


    



/*
public void ClickReview(String myVar, ExtentTest test)
{

    new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@id='MainContent_OEFOnlineRequests_lnkbtnReviewRequest']")));

    HelperMethod.ExplicitWaitByXpath($"//span[contains(text(),'{myVar}')]//ancestor::tbody//a[contains(text(),'مراجعة')]");
    IWebElement Review = driver.FindElement(By.XPath($"//span[contains(text(),'{myVar}')]//ancestor::tbody//a[contains(text(),'مراجعة')]"));

    HelperMethod.clickOnIt(Review, "Review clicked");
    test.Log(Status.Info, "Review Clicked");

}
public void AlertAccept(ExtentTest test)
{
    Thread.Sleep(3000);

    try
    {
        IAlert a = driver.SwitchTo().Alert();

        a.Accept();
        Console.WriteLine("Alert present");
    }

    catch (NoAlertPresentException Ex)
    {
        Console.WriteLine("Alert not present");
        Console.WriteLine(Ex.Message);

    }
}
public void AlertCancel(ExtentTest test)
{
    Thread.Sleep(3000);
    try
    {
        IAlert a = driver.SwitchTo().Alert();

        a.Dismiss();
        Console.WriteLine("Alert present");
    }

    catch (NoAlertPresentException Ex)
    {
        Console.WriteLine("Alert not present");
        Console.WriteLine(Ex.Message);

    }
}
public void ClickTabTypeBusinessActivity(ExtentTest test)
{

    HelperMethod.ExplicitWaitById("MainContent_TabsCompanyInfo1_tab0");
    new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.Id("MainContent_TabsCompanyInfo1_tab0")));

    HelperMethod.ClickOnIt(tabBussinessActivity, "Clicked on Tab tabBussinessActivity");
    test.Log(Status.Info, "Review Clicked");

}

public void ClickTabUnifiedNumber(ExtentTest test)
{
    HelperMethod.ExplicitWaitById("MainContent_TabsCompanyInfo1_lnk_UnifiedNumber");
    HelperMethod.ClickOnIt(tabUnifiedNumber, "Clicked on Tab tabUnifiedNumber");
    test.Log(Status.Info, "tabUnifiedNumber");

}
public void ClickTabLicensingData(ExtentTest test)
{
    HelperMethod.ExplicitWaitById("MainContent_TabsCompanyInfo1_lnk_MainInfo");
    HelperMethod.ClickOnIt(tabLicensingData, "Clicked on Tab tabLicensingData");
    test.Log(Status.Info, "tabLicensingData Clicked");

}

public void ClickTabTitle(ExtentTest test)
{
    HelperMethod.ExplicitWaitById("MainContent_TabsCompanyInfo1_lnk_Address");
    HelperMethod.ClickOnIt(tabTitle, "Clicked on Tab clickTitle");
    test.Log(Status.Info, "clickTitle Clicked");

}


public void ClickBtnNextOne(ExtentTest test)
{
    HelperMethod.ExplicitWaitById("MainContent_EstablishmentEdit1_btnNext");
    HelperMethod.ClickOnIt(btnNext, "Clicked on Tab btnNext");
    test.Log(Status.Info, "btnNext Clicked");

}
public void ClickBtnConsent(ExtentTest test)
{
    HelperMethod.ExplicitWaitById("MainContent_EstablishmentEdit1_btnApprove");
    HelperMethod.ClickOnIt(btnConsent, "Clicked on Tab btnAssent");
    test.Log(Status.Info, "btnConsent Clicked");

}

public void ClickBtnReject(ExtentTest test)
{
    HelperMethod.ExplicitWaitByXpath("//input[@value='رفض']");
    HelperMethod.ClickOnIt(btnReject, "Clicked on Tab btnReject");
    test.Log(Status.Info, "btnReject Clicked");

}

public void EnterRejectDescription(String element, ExtentTest test)
{
    HelperMethod.ExplicitWaitById("MainContent_EstablishmentEdit1_txtRejectionReason");
    HelperMethod.EnterValue(reasonReject, element, "EnterRejectDescription");
    test.Log(Status.Info, "EnterRejectDescription");

}
public void ClickBtnRejectApplication(ExtentTest test)
{
    HelperMethod.ExplicitWaitById("MainContent_EstablishmentEdit1_btnReject");
    HelperMethod.ClickOnIt(btnRejectApplication, "Clicked on Tab btnReject");
    test.Log(Status.Info, "btnReject Clicked");

}
public void ClickBtnCancel(ExtentTest test)
{
    HelperMethod.ExplicitWaitById("MainContent_EstablishmentEdit1_btnReject");
    HelperMethod.ClickOnIt(btnRejectApplication, "Clicked on Tab btnReject");
    test.Log(Status.Info, "btnReject Clicked");

}
*/