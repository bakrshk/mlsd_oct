﻿using AventStack.ExtentReports;
using MlsdTest.Base;
using MlsdTest.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Threading.Tasks;
using MlsdTest.Helper;
using OpenQA.Selenium.Chrome;

namespace MlsdTest.Pages
{
    public class DashBoardPage :BasePage 
    {
        Actions actions;
     private IWebDriver driver;

        
        public DashBoardPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }
        
      
        [FindsBy(How = How.XPath, Using = "/html/body/div[2]/div/div[3]/center/a")]
        IWebElement PopUpAlert { get; set; }

        //------------------------------------------Employment Services - Initialization------------------------------------------//
        [FindsBy(How = How.Id, Using = "div2")]
         IWebElement  cjdList { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_ServiceList1_rptCategories_rptServices_1_lnkStartService_0")]
      
        IWebElement Start_CJD_Btn { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_ServiceList1_rptCategories_rptServices_3_lnkStartService_0")]
        IWebElement Start_LaborTransport_Btn { get; set; }

        [FindsBy(How = How.XPath, Using = ".//div[@id=\"closeall\"]/center/a")]

        IWebElement ClosePopUp { get; set; }

        //------------------------------------------Labor Transport Service - Initialization------------------------------------------//

        [FindsBy(How = How.Id, Using = "div4")]
        IWebElement LaborTransportServiceList { get; set; }
        [FindsBy(How = How.XPath, Using = "//span[@id='Menu1_rp_Groups_lblServices_0']")]
        IWebElement lbService { get; set; }
        [FindsBy(How = How.XPath, Using = "(//div[@class='collapsible-header'])[6]")]

        IWebElement lbServiceChild { get; set; }


        [FindsBy(How = How.XPath, Using = "//a[@id='MainContent_ManpowerLandingControl_rp_Groups_rp_ServicesTitles_0_rp_ServicesPrivileges_1_hlnk_item_0']")]
        IWebElement lbServiceChild2 { get; set; }



        [FindsBy(How = How.XPath, Using = "//a[@id='MainContent_ManpowerLandingControl_rp_Groups_rp_ServicesTitles_0_rp_ServicesPrivileges_1_hlnk_item_3']")]
        IWebElement addAccountManager { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@id='MainContent_ManpowerLandingControl_rp_Groups_rp_ServicesTitles_0_rp_ServicesPrivileges_1_hlnk_item_4']")]
        IWebElement add_establish_user { get; set; }

        //-----------------------------------------------Transfer Service Elements---------------------------------------------------------------//
        [FindsBy(How = How.XPath, Using = "//span[@id = 'Menu1_rp_Groups_lblServices_4' and text() ='خدمات نقل الخدمة']")]
        IWebElement transferService { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//a[@class = 'yay-sub-toggle']//span[text() ='خدمة نقل خدمة التابعين']")]
        IWebElement transferServiceChild { get; set; }


        [FindsBy(How = How.XPath, Using = "//a[@id = 'Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_1_hlnk_item_0' and text() ='الموافقة على طلب نقل خدمة تابع']")]
        IWebElement approveDependantService { get; set; }


        //------------------------View New File Data path------------------

        [FindsBy(How = How.XPath, Using = "//ul[@id='EservicesSideMenu_LoggedInSideMenuView']/li[3]/a[1]")]
        IWebElement facilityManagementService { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@class = 'yay-sub-toggle']//span[contains(text(),'إدارة بيانات المنشآت')]")]
        IWebElement manageEnterpriseData { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'عرض صندوق طلبات فتح ملف منشأة')]")]
        IWebElement viewNewFile { get; set; }


        //------------------------Transfer Service------------------


        [FindsBy(How = How.XPath, Using = "//a[@class = 'yay-sub-toggle']//span[contains(text(),'خدمات نقل الخدمة')]")]
        IWebElement transferservices { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@class = 'yay-sub-toggle']//span[contains(text(),'خدمة نقل خدمة عامل وافد')]")]
        IWebElement inComingAgentService { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@id='Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_0_hlnk_item_1' and contains(text(),'تقديم طلب نقل خدمة عامل وافد')]")]
        IWebElement submitRequestTransfer { get; set; }


        //MainContent_IndividualSponsorTransfer_divError

        //
        //------------------------------------------Employment Services------------------------------------------//
        public void Employment_Service_List(String Service,ExtentTest test)
        {
            
            //driver.Navigate().Refresh();
            ClosePopUp.Click();
            test.Log(Status.Info, "Clicked on PopUp Close button");
            new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementIsVisible(By.Id("div2")));
            WebElementExtension.ListElement(cjdList, Service);
            test.Log(Status.Info, "Clicked on Career change service");
            new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementIsVisible(By.Id("MainContent_ServiceList1_rptCategories_rptServices_1_lnkStartService_0")));
            Start_CJD_Btn.Click();
            test.Log(Status.Info, "Clicked on Start service button");
            new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementIsVisible(By.Id("MainContent_UCChangeLaborerJob_txtLaborerIdNo")));
        }
        public void Labor_Transport_ServiceList(String Service, ExtentTest test)
        {

            //driver.Navigate().Refresh();
            ClosePopUp.Click();
            test.Log(Status.Info, "Closed the Popup Window");
            new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementIsVisible(By.Id("div4")));
            WebElementExtension.ListElement(LaborTransportServiceList,Service);
            test.Log(Status.Info, "Clicked on Labor Transport service");
            new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementIsVisible(By.Id("MainContent_ServiceList1_rptCategories_rptServices_3_lnkStartService_0")));

            actions = new Actions(driver);
            actions.MoveToElement(Start_LaborTransport_Btn);
            actions.Perform();
            Start_LaborTransport_Btn.Click();
            test.Log(Status.Info, "Clicked on Start Labor Transport service button");
            new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementIsVisible(By.Id("MainContent_SponsorTransfer1_txtLaborerIdNo")));


            
        }
        public void clickManagementeEstablishments1()
        {


            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));

            lbService.Click();

        }

        public void FacilityManagementRepresentative(ExtentTest test)
        {


            new WebDriverWait(driver, TimeSpan.FromSeconds(1000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//span[@id='Menu1_rp_Groups_lblServices_0']")));
            //HelperMethod.FluentWaitByXpath("//a[@id = 'Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_1_hlnk_item_0' and text() ='الموافقة على طلب نقل خدمة تابع']");
            HelperMethod.ClickOnIt(lbService, "FacilityManagementRepresentative");
            test.Log(Status.Debug, "Click FacilityManagementRepresentative ");
        }

            public void clickServicesEstablishment()
        {
            lbServiceChild.Click();
        }
        public void clickChangeUnified()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(2000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@id='MainContent_ManpowerLandingControl_rp_Groups_rp_ServicesTitles_0_rp_ServicesPrivileges_1_hlnk_item_0']")));

            lbServiceChild2.Click();
            Thread.Sleep(3000);
        }
        public void clickaddAccountManager()
        {

             new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@id='MainContent_ManpowerLandingControl_rp_Groups_rp_ServicesTitles_0_rp_ServicesPrivileges_1_hlnk_item_3']")));
            //ExplicitWaitByXpath(driver, "//a[@id='Menu1_rp_Groups_rp_ServicesTitles_0_rp_ServicesPrivileges_1_hlnk_item_3']");
            addAccountManager.Click();
        }

        public void clickaddEstablishUser()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@id='MainContent_ManpowerLandingControl_rp_Groups_rp_ServicesTitles_0_rp_ServicesPrivileges_1_hlnk_item_4']")));
            add_establish_user.Click();
        }
        public void GotoUrl(String url)
        {
            Thread.Sleep(1500);
            driver.Navigate().GoToUrl(url);
        }


        // Transfer Service Menu
        public void ClickTransferServicesSidePanel(ExtentTest test)
        {
            HelperMethod.ExplicitWaitByXpath(driver,"//span[@id = 'Menu1_rp_Groups_lblServices_4' and text() ='خدمات نقل الخدمة']");
            HelperMethod.ClickOnIt(transferService, "Transfer Service Span");
            test.Log(Status.Info, "Clicked on transfer service  ");

        }
        //Transfer service dropdown element click
        public void ClickTransferServicesChild(ExtentTest test)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(1000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@class = 'yay-sub-toggle']//span[text() ='خدمة نقل خدمة التابعين']")));

            HelperMethod.ClickOnIt(transferServiceChild, "Transfer child Service Span");
            test.Log(Status.Info, "Clicked on transfer service child ");

        }

        public void ClickApproveDependantService(ExtentTest test)
        {
            //new WebDriverWait(driver, TimeSpan.FromSeconds(1000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@id = 'Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_1_hlnk_item_0' and text() ='الموافقة على طلب نقل خدمة تابع']")));
            HelperMethod.FluentWaitByXpath(driver,"//a[@id = 'Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_1_hlnk_item_0' and text() ='الموافقة على طلب نقل خدمة تابع']");
            HelperMethod.ClickOnIt(approveDependantService, "Approve Dependant Service clicked child of transfer child");
            test.Log(Status.Debug, "Click Approve Dependant Service ");

        }

        //---------------------Facilities management and its representatives----------------------------------------------------//

        public void ClickFacilityManagementRepresentative(ExtentTest test)
        {
            HelperMethod.ExplicitWaitByXpath(driver,"//ul[@id='EservicesSideMenu_LoggedInSideMenuView']/li[3]/a[1]");
            HelperMethod.ClickOnIt(facilityManagementService, "facilityManagementService");
            test.Log(Status.Info, "Clicked on transfer service  ");

        }
        public void ClickManagerEnterprise(ExtentTest test)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(2000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@class = 'yay-sub-toggle']//span[contains(text(),'إدارة بيانات المنشآت')]")));

            HelperMethod.ClickOnIt(manageEnterpriseData, "Transfer child Service Span");
            test.Log(Status.Info, "Clicked on transfer service child ");

        }

        public void ClickViewNewFile(ExtentTest test)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(text(),'عرض صندوق طلبات فتح ملف منشأة')]")));

            HelperMethod.ClickOnIt(viewNewFile, "Approve Dependant Service clicked child of transfer child");
            test.Log(Status.Fatal, "Click Approve Dependant Service ");

        }

        //---------------------Facilities management and its representatives---------------------------------------------//
        public void ClickTransferservices(ExtentTest test)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@class = 'yay-sub-toggle']//span[contains(text(),'خدمات نقل الخدمة')]")));

            HelperMethod.ClickOnIt(transferservices, "transferservices Service");
            test.Log(Status.Fatal, "Clicktransferservices Service ");

        }
        public void ClickInComingAgentService(ExtentTest test)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@class = 'yay-sub-toggle']//span[contains(text(),'خدمة نقل خدمة عامل وافد')]")));

            HelperMethod.ClickOnIt(inComingAgentService, "inComingAgentService Service");
            test.Log(Status.Fatal, "clicked inComingAgentService Service ");

        }

        public void ClickSubmitRequestTransfer(ExtentTest test)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@id='Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_0_hlnk_item_1' and contains(text(),'تقديم طلب نقل خدمة عامل وافد')]")));

            HelperMethod.ClickOnIt(submitRequestTransfer, "submitRequestTransfer Service");
            test.Log(Status.Fatal, "submitRequestTransfer Service ");

        }
    }

}

