﻿using AventStack.ExtentReports;
using MlsdTest.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using MlsdTest.Helper;
using OpenQA.Selenium.Chrome;

namespace MlsdTest.Pages
{
    public class AddAccountManagerPage : BasePage
    {
         private IWebDriver driver;

        public object SeleniumExtras { get; private set; }


        public AddAccountManagerPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }
        
        /* Service 2 Paths Al mansha
        * 
        */


        [FindsBy(How = How.Name, Using = "ctl00$MainContent$txtOfficeId")]
        public IWebElement facility_num { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$txtSeqNumber")]
        public IWebElement facility_num2_sequence { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='MainContent_txtIdNo']")]
        public IWebElement facility_Id_num { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='MainContent_btnCheck']")]
        public IWebElement check_btn_s2 { get; set; }



        [FindsBy(How = How.XPath, Using = "//span[@class='select2 select2-container select2-container--default select2-container--below']//span[@class='select2-selection select2-selection--single']")]
        public IWebElement statusUser { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='MainContent_smsConfirmation_txtMobileCode']")]
        public IWebElement otp_input { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='MainContent_smsConfirmation_btnSubmit']")]
        public IWebElement btn_submit_otp { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@id='MainContent_btnSave']")]
        public IWebElement btn_save_s2 { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='MainContent_smsConfirmation_divSMSError']")]
        public IWebElement errorLabel { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$txtRequesterIdNo")]
        public IWebElement request_service_name { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@id='MainContent_calRoleEndDate_ddldateCulrture']")]
        public IWebElement select_date_culture { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@id='MainContent_calRoleEndDate_ddlYear']")]
        public IWebElement select_date_year { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@id='MainContent_calRoleEndDate_ddlMonth']")]
        public IWebElement select_date_month { get; set; }

        [FindsBy(How = How.XPath, Using = "//table[@id='CalendarTopTable']//tbody//tr//td//input")]
        public IWebElement close_btn { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@class='SmartCalendarTextbox']")]
        public IWebElement txtdropdownCalandar { get; set; }

        public void selectDatelist(String myVar)
        {

            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(text(),'11')]")));

            IWebElement dateselect = driver.FindElement(By.XPath($"//a[contains(text(), '{myVar}')]"));
            Thread.Sleep(3000);

            dateselect.Click();
            //Thread.Sleep(3000);
        }
        public void clickOndate(String calendertype ,ExtentTest test)
        {
            
            //HelperMethod.ExplicitWaitByXpath(driver,"//input[@class='SmartCalendarTextbox']");
            HelperMethod.ExplicitWaitByXpath(driver, "//input[@class='SmartCalendarTextbox']");
            txtdropdownCalandar.Click();
            
            test.Log(Status.Info, "Clicked on Calender ");


            new SelectElement(select_date_culture).SelectByText(calendertype);
            test.Log(Status.Info, "Claender type changed");
         

        }

        public void selectMonth(String month, ExtentTest test)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//select[@id='MainContent_calRoleEndDate_ddlMonth']")));
            Thread.Sleep(5000);
            new SelectElement(select_date_month).SelectByText(month);
            test.Log(Status.Info, "Claender month selected");
        }
        public void selectYear(String year, ExtentTest test)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(10000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//select[@id='MainContent_calRoleEndDate_ddlYear']")));
            Thread.Sleep(5000);
            new SelectElement(select_date_year).SelectByText(year);
            test.Log(Status.Info, "Claender year selected");

            Thread.Sleep(5000);
        }




        public void enterServiceName(string request)
        {
            //new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementIsVisible(By.Name("ctl00$MainContent$txtRequesterIdNo")));

            request_service_name.SendKeys(request);
    

        }


        public void enterFacilityNum(string facilitynum, string facility_num2)
        {
            facility_num.SendKeys(facilitynum);
            facility_num2_sequence.SendKeys(facility_num2);
            //new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//input[@id='MainContent_txtIdNo']")));


        }
        public void enterFacilityIdNum(string facility_id)
        {
            facility_Id_num.SendKeys(facility_id);
        }

        public void clickSearchS2()
        {

            check_btn_s2.Click();
            //ExplicitWaitByXpath(driver, "//div[@id='MainContent_divPrivilegeInFo']//div[1]//div[2]");
            //new WebDriverWait(driver, TimeSpan.FromSeconds(1000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='MainContent_btnSave']")));

        }
        public void clickSaveBtn()
        {

            btn_save_s2.Click();
//new WebDriverWait(driver, TimeSpan.FromSeconds(10000)).Until(ExpectedConditions.ElementIsVisible(By.XPath(" //input[@id='MainContent_smsConfirmation_btnSubmit']")));

        }

        public void enterOtpNum(String otp_num)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(10000)).Until(ExpectedConditions.ElementIsVisible(By.XPath(" //input[@id='MainContent_smsConfirmation_btnSubmit']")));
            otp_input.SendKeys(otp_num);

        }
        public void clickOtpBtn()
        {
            btn_submit_otp.Click();
            new WebDriverWait(driver, TimeSpan.FromSeconds(5000)).Until(ExpectedConditions.ElementIsVisible(By.XPath(" //input[@id='MainContent_smsConfirmation_btnSubmit']")));

        }



    }

}
