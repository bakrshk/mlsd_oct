﻿using AventStack.ExtentReports;
using MlsdTest.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using MlsdTest.Helper;

using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SeleniumWebdriverHelpers;
using OpenQA.Selenium.Chrome;

namespace MlsdTest.Pages
{
    public class LoginMyClientPage : BasePage
    {

        private IWebDriver driver;
        public LoginMyClientPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }
        
        //ExtentTest test;
        /* Locators of LoginPage
         * 
         */

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$MPLogin1$txtIDNumber")]
        public IWebElement txt_UserName { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$MPLogin1$txtPassword")]
        public IWebElement txt_Password { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_MPLogin1_btnLogin")]
        public IWebElement btn_Submit { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$MPLogin1$txtConfirmPassword")]
        public IWebElement txt_Confirm_Password { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_MPLogin1_btnConfirmPassword")]
        public IWebElement btn_ConfirmPassword { get; set; }

        [FindsBy(How = How.XPath, Using = ".//div[@class=\"input-field\"]/label")]
        public IWebElement teste { get; set; }
        public void enterUsername(String username)
        {

            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementIsVisible(By.Name("ctl00$MainContent$MPLogin1$txtIDNumber")));
            txt_UserName.Clear();
            txt_UserName.SendKeys(username);
           // HelperMethod.ExplicitWaitById("");

        }
        public void enterPassword(String password)
        {
            txt_Password.Clear();
            txt_Password.SendKeys(password);
            
            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementIsVisible(By.Id("MainContent_MPLogin1_btnLogin")));

        }

    
            public void clickSubmit(String password, ExtentTest test)
            {
                btn_Submit.Click();
                new WebDriverWait(driver, TimeSpan.FromSeconds(10000)).Until(ExpectedConditions.ElementIsVisible(By.Id("MainContent_MPLogin1_btnConfirmPassword")));
            //ExplicitWaitByXpath(driver, ".//div[@class=\"input-field\"]/label");
            Thread.Sleep(3000);

            if (teste.Text == "كلمة المرور")
                {


                    txt_Confirm_Password.Clear();
                    txt_Confirm_Password.SendKeys(password);
                    test.Log(Status.Info, "Entered confirm Password in to the field");
                    btn_ConfirmPassword.Click();

                }
                else
                {
                    Console.WriteLine("No confirmation");
                }
                Thread.Sleep(3000);
            }

            public void Login_to_MyServices(String username, String password, ExtentTest test)
       {
            //IWebElement username1 = find_element(driver , By.Name("ctl00$MainContent$MPLogin1$txtIDNumber"));
           txt_UserName.Clear();
           txt_UserName.SendKeys(username);
           test.Log(Status.Info, "User name entered into the field");
           txt_Password.Click();
           txt_Password.Clear();
           txt_Password.SendKeys(password);

           test.Log(Status.Info, "Entered Password int to the field");
           test.Log(Status.Info, "Entered Captcha");
           btn_Submit.Click();
           test.Log(Status.Info, "Clicked on Submit button");

           new WebDriverWait(driver, TimeSpan.FromSeconds(10000)).Until(ExpectedConditions.ElementIsVisible(By.Id(".//div[@class=\"input-field\"]/label")));

           if (teste.Text == "كلمة المرور")
           {
               txt_Confirm_Password.Clear();
               txt_Confirm_Password.SendKeys(password);
               test.Log(Status.Info, "Entered confirm Password in to the field");
               btn_ConfirmPassword.Click();

           }
           test.Log(Status.Info, "Entered Activation Code");
           driver.Navigate().Refresh();
           new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#form2 > section > div.page-title > div > div > h1")));
           test.Log(Status.Info, "User is logged in successfully");
       }







   /*

   [FindsBy(How = How.Name, Using = "ctl00$MainContent$Login1$ucCaptcha$txtCaptchaCode")]
   public IWebElement txt_Captcha { get; set; }

   [FindsBy(How = How.Name, Using = "ctl00$MainContent$Login1$txtSMSCode")]
   public IWebElement txt_ActivationCode { get; set; }

   [FindsBy(How = How.CssSelector, Using = "#MainContent_Login1_btnSMSCodeValidate")]
   public IWebElement btn_Login { get; set; }

   [FindsBy(How = How.Name, Using = "ctl00$MainContent$Login1$txtConfirmPassword")]
   public IWebElement txt_ConfirmPassword { get; set; }



   [FindsBy(How = How.XPath, Using = ".//div[@class=\"input-field\"]/label")]
   public IWebElement teste { get; set; }


    */




    }
}


