﻿using AventStack.ExtentReports;
using MlsdTest.Base;
using MlsdTest.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MlsdTest.Pages
{
    public class HomePage : BasePage
    {
      private IWebDriver driver;

        
        public HomePage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }
        
        //------------------------------------------initialization------------------------------------------//

        [FindsBy(How = How.Id, Using = "toggle")]
        public IWebElement btn_Dropdown  { get; set; }

        [FindsBy(How = How.Id, Using = "LoggedInView1_lnkbtnLogout")]
        public IWebElement btn_Logout { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#form2 > section > div.page-title > div > div > h1")]
        public IWebElement homePage_Title { get; set; }

        [FindsBy(How = How.Id, Using = "select2-MainContent_EstablishmentFilter1_ddlEstablishments-container")]
        IWebElement btn_OpenDropDownlist { get; set; }

        [FindsBy(How = How.Id, Using = "select2-MainContent_EstablishmentFilter1_ddlEstablishments-results")]
        IWebElement Facility_List { get; set; }


        [FindsBy(How = How.ClassName, Using = "count")]
        IWebElement Start_Btn{ get; set; }


        //------------------------------------------Functions------------------------------------------//


        public void StartService(string dropdownvalue,ExtentTest test)
        {
           
            btn_OpenDropDownlist.Click();
            test.Log(Status.Info, "Clicked on Facility List");
            WebElementExtension.ListElement(Facility_List, dropdownvalue);
            test.Log(Status.Info, dropdownvalue+" Facility Selected ");
            Start_Btn.Click();
            test.Log(Status.Info, "Clicked on Start button");
        }

        public void click_Logout(ExtentTest test)
        {
            btn_Dropdown.Click();
            test.Log(Status.Info, "Clicked on Dropdown button");
            btn_Logout.Click();
            test.Log(Status.Info, "Clicked on LogOut button");

        }

        public string verifyllogin()
        {
            string pagetitle=WebElementExtension.GetText(homePage_Title);
            return pagetitle;
        }






        
    }
}
