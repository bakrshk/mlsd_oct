﻿
using AventStack.ExtentReports;
using MlsdTest.Base;
using MlsdTest.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MlsdTest.Helper;
using OpenQA.Selenium.Chrome;

namespace MlsdTest.Pages
{
    public class ApprovalRequestPage : BasePage
    {
        Actions actions;
        private IWebDriver driver;

        public object SeleniumExtras { get; private set; }

        public ApprovalRequestPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }


        //------------------------Approve a dependent service transfer request------------------

        [FindsBy(How = How.Id, Using = "MainContent_IndividualSponsorTransfer_ucValidateBusinessUser_txtEstOfficeId")]
        IWebElement establishment_labor { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_IndividualSponsorTransfer_ucValidateBusinessUser_txtEstSequenceNo")]
        IWebElement establishment_number { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_IndividualSponsorTransfer_ucValidateBusinessUser_txtReuesterIdNo")]
        IWebElement establishment_representative   { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$IndividualSponsorTransfer$txtSearchIdNo")]
        IWebElement id_number   { get; set; }
        [FindsBy(How = How.Name, Using = "ctl00$MainContent$IndividualSponsorTransfer$ucValidateBusinessUser$btnValidate")]
        IWebElement check_btn { get; set; }


        [FindsBy(How = How.Id, Using = "MainContent_IndividualSponsorTransfer_btnApprove")]
        IWebElement approve_btn { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_IndividualSponsorTransfer_btnReject")]
        IWebElement reject_btn { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$IndividualSponsorTransfer$btnSearch")]
        IWebElement search_btn { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_IndividualSponsorTransfer_divError")]
        IWebElement Error { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_IndividualSponsorTransfer_ucValidateBusinessUser_lblErrorMessage")]
        IWebElement Error2 { get; set; }


        //---------------------------------------Methods-------------------------------------------------//

        public void EnterEstablishmentLabor(String element ,ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_IndividualSponsorTransfer_ucValidateBusinessUser_txtEstOfficeId");
            HelperMethod.EnterValue(establishment_labor, element, "Labor Value Entered");
            test.Log(Status.Info, "Labor Value Entered");

        }
        //Transfer service dropdown element click
          public void EnterEstablishmentNumber(String element ,ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_IndividualSponsorTransfer_ucValidateBusinessUser_txtEstSequenceNo");
            HelperMethod.EnterValue(establishment_number, element, "Establishment value Entered");
            test.Log(Status.Info, "Establishment value Entered");

        }

        public void EnterEstablishmentRepresentative(String element ,ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_IndividualSponsorTransfer_ucValidateBusinessUser_txtReuesterIdNo");
            HelperMethod.EnterValue(establishment_representative, element, "Establishment Representative Entered");
            test.Log(Status.Info, "Establishment Representative Entered");

        }
        public void ClickCheckButton(ExtentTest test)
        {
            Thread.Sleep(2000);
            //new WebDriverWait(driver, TimeSpan.FromSeconds(1000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@id = 'Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_1_hlnk_item_0' and text() ='الموافقة على طلب نقل خدمة تابع']")));
            HelperMethod.FluentWaitByName(driver, "ctl00$MainContent$IndividualSponsorTransfer$ucValidateBusinessUser$btnValidate");
            HelperMethod.ClickOnIt(check_btn, "Check Button clicked");
            test.Log(Status.Debug, "Clicked Check");

        }

        public void EnterIdNumber(String element, ExtentTest test)
        {
            HelperMethod.ExplicitWaitByName(driver, "ctl00$MainContent$IndividualSponsorTransfer$txtSearchIdNo");
            HelperMethod.EnterValue(id_number, element, "EnterIdNumber");
            test.Log(Status.Info, "EnterIdNumber");

        }
        public void ClickApproveRequest(ExtentTest test)
        {
            //new WebDriverWait(driver, TimeSpan.FromSeconds(1000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@id = 'Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_1_hlnk_item_0' and text() ='الموافقة على طلب نقل خدمة تابع']")));
            HelperMethod.ExplicitWaitById(driver, "MainContent_IndividualSponsorTransfer_btnApprove");
            HelperMethod.ClickOnIt(approve_btn, "Approve Dependant Service Approve Button clicked");
            test.Log(Status.Debug, "Approve Dependant Service Button clicked");

        }

        public void ClickRejectRequest(ExtentTest test)
        {
            //new WebDriverWait(driver, TimeSpan.FromSeconds(1000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@id = 'Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_1_hlnk_item_0' and text() ='الموافقة على طلب نقل خدمة تابع']")));
            HelperMethod.ExplicitWaitById(driver, "MainContent_IndividualSponsorTransfer_btnApprove");
            HelperMethod.ClickOnIt(reject_btn, "Approve Dependant Service Rejected  Button clicked");
            test.Log(Status.Debug, "Approve Dependant Service Button clicked");

        }
        public void ClickSearch(ExtentTest test)
        {
            //new WebDriverWait(driver, TimeSpan.FromSeconds(1000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@id = 'Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_1_hlnk_item_0' and text() ='الموافقة على طلب نقل خدمة تابع']")));
            HelperMethod.ExplicitWaitByName(driver, "ctl00$MainContent$IndividualSponsorTransfer$btnSearch");
            HelperMethod.ClickOnIt(search_btn, "Clicked searched");
            test.Log(Status.Debug, "Clicked searched");

        }
        public void ErrorMethod(ExtentTest test)
        {
            try
            {

                //new WebDriverWait(driver, TimeSpan.FromSeconds(1000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@id = 'Menu1_rp_Groups_rp_ServicesTitles_4_rp_ServicesPrivileges_1_hlnk_item_0' and text() ='الموافقة على طلب نقل خدمة تابع']")));
                HelperMethod.ExplicitWaitByName(driver, "ctl00$MainContent$IndividualSponsorTransfer$btnSearch");
                HelperMethod.ClickOnIt(search_btn, "Clicked searched");
                test.Log(Status.Debug, "Clicked searched");

            }
            catch (NoAlertPresentException ex)
            {
                // Alert not present
                // Check the presence of alert
                IAlert alert = driver.SwitchTo().Alert();
                // Alert present; set the flag
                // if present consume the alert
                alert.Accept();
            }


          

        }

    }
}