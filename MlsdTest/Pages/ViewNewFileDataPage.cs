﻿
using AventStack.ExtentReports;
using MlsdTest.Base;
using MlsdTest.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MlsdTest.Helper;
using OpenQA.Selenium.Chrome;

namespace MlsdTest.Pages
{
    public class ViewNewFileDataPage : BasePage
    {
        Actions actions;
        private IWebDriver driver;

        public object SeleniumExtras { get; private set; }

        public ViewNewFileDataPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }


        //------------------------Approve a dependent service transfer request------------------

        
        [FindsBy(How = How.Id, Using = "MainContent_TabsCompanyInfo1_lnk_MainInfo")]
        IWebElement tabBussinessActivity { get; set; }
  
        [FindsBy(How = How.Id, Using = "MainContent_TabsCompanyInfo1_lnk_UnifiedNumber")]
        IWebElement tabUnifiedNumber { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_TabsCompanyInfo1_lnk_Lisenses")]
        IWebElement tabLicensingData { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_EstablishmentEdit1_btnNext")]
        IWebElement btnNext { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_TabsCompanyInfo1_lnk_Address")]
        IWebElement tabTitle { get; set; }


        [FindsBy(How = How.Id, Using = "MainContent_EstablishmentEdit1_btnApprove")]
        IWebElement btnConsent { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value='رفض']")]
        IWebElement btnReject { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_EstablishmentEdit1_txtRejectionReason")]
        IWebElement reasonReject { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_EstablishmentEdit1_btnReject")]
        IWebElement btnRejectApplication { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_EstablishmentEdit1_btnCancelReservation")]
        IWebElement btnCancel { get; set; }


        public void ClickReview(String myVar , ExtentTest test)
        {

            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@id='MainContent_OEFOnlineRequests_lnkbtnReviewRequest']")));

            HelperMethod.ExplicitWaitByXpath(driver, $"//span[contains(text(),'{myVar}')]//ancestor::tbody//a[contains(text(),'مراجعة')]");
            IWebElement Review = driver.FindElement(By.XPath($"//span[contains(text(),'{myVar}')]//ancestor::tbody//a[contains(text(),'مراجعة')]"));

            HelperMethod.clickOnIt(Review, "Review clicked");
            test.Log(Status.Info, "Review Clicked");

        }
        public void AlertAccept(ExtentTest test)
        {
            Thread.Sleep(3000);

            try
            {
                IAlert a = driver.SwitchTo().Alert();

                a.Accept();
                Console.WriteLine("Alert present");
            }

            catch(NoAlertPresentException Ex)
            {
                Console.WriteLine("Alert not present");
                Console.WriteLine(Ex.Message);

            }
        }
        public void AlertCancel(ExtentTest test)
        {
            Thread.Sleep(3000);
            try
            {
                IAlert a = driver.SwitchTo().Alert();

                a.Dismiss();
                Console.WriteLine("Alert present");
            }

            catch (NoAlertPresentException Ex)
            {
                Console.WriteLine("Alert not present");
                Console.WriteLine(Ex.Message);

            }
        }
        public void ClickTabTypeBusinessActivity(ExtentTest test)
        {

            HelperMethod.ExplicitWaitById(driver, "MainContent_TabsCompanyInfo1_tab0");
            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementToBeClickable(By.Id("MainContent_TabsCompanyInfo1_tab0")));

            HelperMethod.ClickOnIt(tabBussinessActivity, "Clicked on Tab tabBussinessActivity");
            test.Log(Status.Info, "Review Clicked");

        }

        public void ClickTabUnifiedNumber(ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_TabsCompanyInfo1_lnk_UnifiedNumber");
            HelperMethod.ClickOnIt(tabUnifiedNumber, "Clicked on Tab tabUnifiedNumber");
            test.Log(Status.Info, "tabUnifiedNumber");

        }
        public void ClickTabLicensingData(ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_TabsCompanyInfo1_lnk_MainInfo");
            HelperMethod.ClickOnIt(tabLicensingData, "Clicked on Tab tabLicensingData");
            test.Log(Status.Info, "tabLicensingData Clicked");

        }

        public void ClickTabTitle(ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_TabsCompanyInfo1_lnk_Address");
            HelperMethod.ClickOnIt(tabTitle, "Clicked on Tab clickTitle");
            test.Log(Status.Info, "clickTitle Clicked");

        }


        public void ClickBtnNextOne(ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_EstablishmentEdit1_btnNext");
            HelperMethod.ClickOnIt(btnNext, "Clicked on Tab btnNext");
            test.Log(Status.Info, "btnNext Clicked");

        }
        public void ClickBtnConsent(ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_EstablishmentEdit1_btnApprove");
            HelperMethod.ClickOnIt(btnConsent, "Clicked on Tab btnAssent");
            test.Log(Status.Info, "btnConsent Clicked");

        }

        public void ClickBtnReject(ExtentTest test)
        {
            HelperMethod.ExplicitWaitByXpath(driver, "//input[@value='رفض']");
            HelperMethod.ClickOnIt(btnReject, "Clicked on Tab btnReject");
            test.Log(Status.Info, "btnReject Clicked");

        }

        public void EnterRejectDescription(String element, ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_EstablishmentEdit1_txtRejectionReason");
            HelperMethod.EnterValue(reasonReject, element, "EnterRejectDescription");
            test.Log(Status.Info, "EnterRejectDescription");

        }
        public void ClickBtnRejectApplication(ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_EstablishmentEdit1_btnReject");
            HelperMethod.ClickOnIt(btnRejectApplication, "Clicked on Tab btnReject");
            test.Log(Status.Info, "btnReject Clicked");

        }
        public void ClickBtnCancel(ExtentTest test)
        {
            HelperMethod.ExplicitWaitById(driver, "MainContent_EstablishmentEdit1_btnReject");
            HelperMethod.ClickOnIt(btnRejectApplication, "Clicked on Tab btnReject");
            test.Log(Status.Info, "btnReject Clicked");

        }
    }
}
 