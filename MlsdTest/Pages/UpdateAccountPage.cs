﻿using AventStack.ExtentReports;
using MlsdTest.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using SeleniumWebdriverHelpers;
using OpenQA.Selenium.Chrome;
using MlsdTest.Helper;

namespace MlsdTest.Pages
{
    public class UpdateAccountPage : BasePage
    {

        private IWebDriver driver;
        
        public UpdateAccountPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }
        

        // lOCATORS OF Change the unified account number manager

        [FindsBy(How = How.XPath, Using = "//input[@id='MainContent_txtRequesterIdNo']")]
        public IWebElement student_service1 { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$txtUNOfficeId")]
        public IWebElement unified_number2 { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$txtUnifiedSeqNumber")]
        public IWebElement unified_number3 { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$btnSearchCurrentUNAM")]
        public IWebElement btn_search { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$txt_NewAM_IDNO")]
        public IWebElement new_id_number { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$txtFirstName")]
        public IWebElement first_name { get; set; }

        [FindsBy(How = How.Name, Using = "ctl00$MainContent$txtFourthName")]
        public IWebElement last_name { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@class='SmartCalendarTextbox']")]
        public IWebElement date_input { get; set; }

        [FindsBy(How = How.Name, Using = "MainContent_calBirthDate_ddldateCulrture")]
        public IWebElement select_date_culture { get; set; }

        [FindsBy(How = How.XPath, Using = "//select[@id='MainContent_calBirthDate_ddlYear']")]
        public IWebElement select_date_year { get; set; }


        [FindsBy(How = How.XPath, Using = "//select[@id='MainContent_calBirthDate_ddlMonth']")]
        public IWebElement select_date_month { get; set; }
        [FindsBy(How = How.XPath, Using = "//body//input[7]")]
        public IWebElement close_btn { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='select2-selection select2-selection--single']")]
        public IWebElement select_nationality { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='MainContent_btnSearchNewUNAccountManager']")]
        public IWebElement btn_search_with_data { get; set; }

        // Last Code Date Elements
        [FindsBy(How = How.XPath, Using = "//input[@class='SmartCalendarTextbox']")]
        public IWebElement txtdropdownCalandar { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_BasicInfo1_scManageUserProfileBirthDate_ddldateCulrture")]
        public IWebElement selectCalenderType { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_BasicInfo1_scManageUserProfileBirthDate_ddlYear")]
        public IWebElement selectYear { get; set; }

        [FindsBy(How = How.Id, Using = "MainContent_BasicInfo1_scManageUserProfileBirthDate_ddlMonth")]
        public IWebElement selectMonth { get; set; }

        [FindsBy(How = How.ClassName, Using = "mol-normal-day")]
        public IWebElement selectDate { get; set; }

        [FindsBy(How = How.Id, Using = "select2-MainContent_ddlNationality-container")]
        public IWebElement clickDropdownNation { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@class='select2-search__field']")]
        public IWebElement dropdownListInput { get; set; }

       

       

        //selectdate from Excel


        public void selectDatelist(String myVar)
        {
            Thread.Sleep(5000);

            HelperMethod.FluentWaitByXpath(driver,$"//a[contains(text(), '{myVar}')]");
            IWebElement dateselect = driver.FindElement(By.XPath($"//a[contains(text(), '{myVar}')]"));
            dateselect.Click();
            Thread.Sleep(1500);

        }
        public void closeBtnDate()
        {
            close_btn.Click();
        }

        public void enterStudentServiceID(String serviceID )
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementIsVisible(By.Name("ctl00$MainContent$txtUNOfficeId")));
            student_service1.SendKeys(serviceID);          
        }
        public void enterUnifiedID(String unified1, String unified2)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(1500)).Until(ExpectedConditions.ElementIsVisible(By.Name("ctl00$MainContent$txtUNOfficeId")));

            unified_number2.SendKeys(unified1);

            unified_number3.SendKeys(unified2);
        }

        public void clickSearchBtn()
        {
            btn_search.Click();
            new WebDriverWait(driver, TimeSpan.FromSeconds(3000)).Until(ExpectedConditions.ElementIsVisible(By.Name("ctl00$MainContent$txt_NewAM_IDNO")));

        }
       
        public void enterNewUnifiedID(String id)
        {
            new_id_number.SendKeys(id);

        }

        public void enterFirstName(String fname)
        {
            first_name.SendKeys(fname);
        }

        public void enterLastname(String lname)
        {
            last_name.SendKeys(lname);
    
        }

        public void clickOndate(String calendertype,
                                  ExtentTest test)
        {

            txtdropdownCalandar.Click();
            test.Log(Status.Info, "Clicked on Calender ");
            new SelectElement(select_date_culture).SelectByText(calendertype);
            test.Log(Status.Info, "Claender type changed");
            Thread.Sleep(5000);
            //ExplicitWaitByXpath("//select[@id='MainContent_calBirthDate_ddlYear' and //option[text()='2019']]");

            new WebDriverWait(driver, TimeSpan.FromSeconds(5000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//select[@id='MainContent_calBirthDate_ddlYear' and //option[text()='2019']]")));


            // List<IWebElement> Date = new List<IWebElement>(driver.FindElements(By.LinkText(date)));
            // driver.FindElement(By.LinkText(date)).Click();

        }
        public void SelectYearMonth(String year, String month, ExtentTest test) {
            new WebDriverWait(driver, TimeSpan.FromSeconds(5000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//select[@name = 'MainContent_calBirthDate_ddlMonth' and //option[text() ='أكتوبر']]")));

            new SelectElement(select_date_month).SelectByText(month);
            new WebDriverWait(driver, TimeSpan.FromSeconds(5000)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//select[@id='MainContent_calBirthDate_ddlYear']")));
            Thread.Sleep(3000);
            new SelectElement(select_date_year).SelectByText(year);
            test.Log(Status.Info, "Calender year selected");
 

            test.Log(Status.Info, "Claender month selected");

            
        }


        public void selectNationailty(string nation , ExtentTest test)
        {
            Thread.Sleep(3000);

            new WebDriverWait(driver, TimeSpan.FromSeconds(25)).Until(ExpectedConditions.ElementIsVisible(By.Id("select2-MainContent_ddlNationality-container")));
            clickDropdownNation.Click();
           // new WebDriverWait(driver, TimeSpan.FromSeconds(25)).Until(ExpectedConditions.ElementIsVisible(By.Id("//input[@class='select2-search__field']")));

//            dropdownListInput.Click();
            dropdownListInput.SendKeys(nation);
            dropdownListInput.SendKeys(Keys.Enter);
            test.Log(Status.Info, "Nation  selected");


        }
        public void Assertion(string nation, ExtentTest test)
        {

            new WebDriverWait(driver, TimeSpan.FromSeconds(25)).Until(ExpectedConditions.ElementIsVisible(By.Id("select2-MainContent_ddlNationality-container")));
            clickDropdownNation.Click();
            Thread.Sleep(3000);
            HelperMethod.ExplicitWaitByXpath(driver,"//input[@class='select2-search__field']");
            //            dropdownListInput.Click();
            dropdownListInput.SendKeys(nation);
            dropdownListInput.SendKeys(Keys.Enter);
            test.Log(Status.Info, "Nation  selected");


        }
        public void clickOnSearchData()
        {
            HelperMethod.ExplicitWaitByXpath(driver,"//input[@id='MainContent_btnSearchNewUNAccountManager']");
            btn_search_with_data.Click();
        }

        public void EnterDescription(String value)
        {

            // ExplicitWaitByName("ctl00$MainContent$txtAccountManagerType");
            HelperMethod.ExplicitWaitByXpath(driver, "//input[@id='MainContent_txtAccountManagerType']");
            IWebElement description = driver.FindElement(By.Name("ctl00$MainContent$txtAccountManagerType"));
            description.SendKeys(value);
        }

        public void EnterMobileNumber(String value)
        {

            IWebElement mobileNumber = driver.FindElement(By.Name("ctl00$MainContent$txtMobileNo"));
            mobileNumber.SendKeys(value);
            Thread.Sleep(5000);
        }

        public void ClickSubmit(ExtentTest test)
        {
            IWebElement Submit = driver.FindElement(By.Name("ctl00$MainContent$btnSaveUNAM"));
            Submit.Click();
            test.Log(Status.Info, "Clicked on submit");

        }
        // Add account Service2 Almansha 
        public void verify(ExtentTest test)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(25)).Until(ExpectedConditions.ElementIsVisible(By.Id("MainContent_lblConfirm")));

            IWebElement confirm = driver.FindElement(By.Id("MainContent_lblConfirm"));
            confirm.GetText();
            test.Log(Status.Info, "Getting Value");

        }
    }

}
