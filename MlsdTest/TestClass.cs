﻿using MlsdTest.Base;
using MlsdTest.Helper;
using MlsdTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest
{
    [TestFixture]
    public class TestClass : BaseClass
    {
        //ExtentReports extent;
        // ExtentTest test;
        public SignUpPage signuppage;
        public HomePage homePage;
        LoginPage loginpage;
        


        //------------------------------------------CreateUser------------------------------------------//

        [Test, Order(1), Category("Smoke")]
        public void CreateUser()
        {

            ExcelHelper.PopulateInCollecetion(@"C:\Users\afshan.shakoor\source\repos\MlsdTest\MlsdTest\Data\Registrationdata.xlsx");

            //------------------------------------------CreateUser-Form1------------------------------------------//

            for (int i = 1; i <= 1; i++)
            {
                loginpage.newUserLink();
                signuppage.enterFirstName(ExcelHelper.ReadData(i, "First Name"));
                signuppage.enterMidName(ExcelHelper.ReadData(i, "Fourth Name"));
                signuppage.selectCalendertype();
                signuppage.selectCalenderYear();
                signuppage.selectCalenderMonth();
                signuppage.enterCaptcha(ExcelHelper.ReadData(i, "Captcha"));
                signuppage.selectNationality(ExcelHelper.ReadData(i, "Nationality"));         
                signuppage.enterIqamaID(ExcelHelper.ReadData(i, "Iqama Id"));
                signuppage.clickSubmitBtn();


           //------------------------------------------CreateUser-Form2------------------------------------------//
                Thread.Sleep(1000);
               // signuppage.enterPassword(ExcelHelper.ReadData(i, "Password"));
                signuppage.enterConfirmPassword(ExcelHelper.ReadData(i, "Confirm Password"));
                signuppage.enterEmail(ExcelHelper.ReadData(i, "Email"));
                signuppage.enterConfirmEmail(ExcelHelper.ReadData(i, "Confirm Email"));
                signuppage.enterMobileNumber(ExcelHelper.ReadData(i, "Mobile #"));
                signuppage.clickSaveButton();
                signuppage.navigateToHomepage();
            }
        }

        [SetUp]
        public void setUp()
        {
            homePage = new HomePage(driver);
            signuppage = new SignUpPage(driver);
            loginpage = new LoginPage(driver);
           

        }
    }

     

    
}
