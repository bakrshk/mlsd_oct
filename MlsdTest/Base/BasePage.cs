﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MlsdTest.Base
{
    public abstract class BasePage
    {
        private static IWebDriver _driver;

        public BasePage()
        {
        }

        public BasePage(IWebDriver _driver)
        {
            PageFactory.InitElements(_driver, this);
        }



    }
}