﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MlsdTest.Pages;
using MlsdTest.Helper;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Reflection;
using OpenQA.Selenium.IE;
using System.Diagnostics;

namespace MlsdTest.Base
{
    public class BaseClass
    {
        
        public LoginMyClientPage loginmyclientpage;
        public HelperMethod helperMethod;

        public HomePage homepage;
        public UpdateAccountPage update;
        public AddAccountManagerPage addManager;
        public UserEstablishbmentPage addEstablish;
        public DashBoardPage dashBoardPage;
        public ExcelHelper excelHelper;
        public ApprovalRequestPage approvalRequest;
        public ViewNewFileDataPage viewNewFileDataPage;
        public TransferServicePage transferServicePage;

        public String url = "http://mol-stage-app3.molstagedom.net:9100/SecureSSL/MyClientsLogin.aspx";  
        public static IWebDriver driver;
        string finalpath,BrowserName;
        public static string ExcelFilepath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        String BrowserType = ExcelFilepath + "\\Data\\Registrationdata.xlsx";

        //------------------------------------------Reporting Variables------------------------------------------//
        public static ExtentReports extent;
        public static ExtentTest test;
        static string FilepathFolder = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
        private static string Filepath = FilepathFolder + "ExecutionResult\\" +"MLSD Automation Report "+ DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".html";
        
        private static ExtentHtmlReporter reportPath = new ExtentHtmlReporter(Filepath);

        public BaseClass()

         {
           
            if (System.IO.File.Exists(Filepath))
                reportPath.AppendExisting = true;
            else
            {
                extent = new ExtentReports();
                reportPath.AppendExisting = true;
                extent.AttachReporter(reportPath);
                extent.AddSystemInfo("Enviornment", " Windows 10 - 64 bit Operating system");
                extent.AddSystemInfo("Username", "Abubakr");


            }
        }
        [OneTimeSetUp]
        
        public void setUp()
        {
            excelHelper = new ExcelHelper();
            excelHelper.PopulateInCollecetion(BrowserType, "Browser");
            BrowserName = excelHelper.ReadData(1, "BrowserName");
            if (BrowserName == "Chrome")
            {
              //  var options = new ChromeOptions();

               // driver = new ChromeDriver(@"D:\MlsdTest\MlsdTest\Extensions");


               // options.AddArgument("no-sandbox");
                //options.AddArgument("--headless");

                ChromeOptions option = new ChromeOptions();
                option.AddArgument("no-sandbox");
                driver = new ChromeDriver(@"D:\", option, TimeSpan.FromSeconds(240));

            }
            else if(BrowserName=="IE")
            {

                var internetExplorerOptions = new InternetExplorerOptions
                {
                    RequireWindowFocus = true,
                    EnablePersistentHover = true,
                    EnableNativeEvents = true,
                    IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                    //IgnoreZoomLevel = true
                };
                driver.Manage().Window.Maximize();
                driver = new InternetExplorerDriver(internetExplorerOptions);

               
              
            }

            //Page Initialization//
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            
            loginmyclientpage = new LoginMyClientPage(driver);
            homepage = new HomePage(driver);
            dashBoardPage = new DashBoardPage(driver);
            update = new UpdateAccountPage(driver);
            addManager = new AddAccountManagerPage(driver);
            addEstablish = new UserEstablishbmentPage(driver);
            approvalRequest = new ApprovalRequestPage(driver);
            viewNewFileDataPage = new ViewNewFileDataPage(driver);
            transferServicePage = new TransferServicePage(driver);

    }




    [OneTimeTearDown]
        public static void FlushReport()
        {
            extent.Flush();
            driver.Quit();

        
        }
        [SetUp]
        public void Initialize_Reports()
        {
          
            test = extent.CreateTest(TestContext.CurrentContext.Test.Name);
            test.Log(Status.Info, TestContext.CurrentContext.Test.Name + " Test Started");
            test.AssignAuthor("Abubakr");
            

        }

        public string Capture(IWebDriver driver)
        {
           finalpath = FilepathFolder+ "\\ExecutionResult\\Screenshots\\" + TestContext.CurrentContext.Test.Name + ".png";
            try
            {
                Thread.Sleep(4000);
                ITakesScreenshot ts = (ITakesScreenshot)driver;
                Screenshot screenshot = ts.GetScreenshot();                
                screenshot.SaveAsFile(finalpath);




            }
            catch (Exception e)
            {
                throw (e);
            }
            return finalpath;
        }

    }
}
