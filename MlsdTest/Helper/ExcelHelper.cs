﻿using ExcelDataReader;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;


namespace MlsdTest.Helper
{
    public class ExcelHelper
    {
        public List<DataCollection> datcollist = new List<DataCollection>();
        public DataTable table;
       
        


        //Storing all excel values into the memory collection//
        public int PopulateInCollecetion(String filename, String sheetNum)
        {
            table = ExceltoDataTable(filename, sheetNum);
            for (int row = 1; row <= table.Rows.Count; row++)
            {

                for (int col = 0; col <= table.Columns.Count - 1; col++)
                {
                    DataCollection dtTable = new DataCollection()
                    {
                        RowNumber = row,
                        ColumnName = table.Columns[col].ColumnName,
                        Cellvalue = table.Rows[row - 1][col].ToString(),
                        ColumnNumber = col
                       
                    };
                    datcollist.Add(dtTable);
                }
                               
            }
            return table.Rows.Count;

        }
        //one time//
        public DataTable ExceltoDataTable(String filename, string sheetNum)
        {
            using (var stream = File.Open(filename, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (data) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }

                    });
                    //Get all the Tables
                    DataTableCollection table = result.Tables;
                    //Store it in DataTable
                    DataTable resultTable = table[sheetNum];
                    //return
                    return resultTable;
                }


            }

        }

        //Reading data from list//
        public string ReadData(int RowNumber, string ColumnName)
        {

            try
            {
                //Retriving Data using LINQ to reduce much of iterations
                string data = (from colDatalist in datcollist
                               where colDatalist.ColumnName == ColumnName && colDatalist.RowNumber == RowNumber
                               select colDatalist.Cellvalue).SingleOrDefault();
                return data.ToString();
                
            }
            catch (Exception e)
            {
                return null;
            }


        }

      

        public void writedata(String filename,String Sheetname,int column,int row,string Status)
        {

            Excel.Application oXL=null;
            Excel.Workbook oWB = null;
            Excel._Worksheet oSheet = null;
            try {
                Console.WriteLine(filename);

                oXL = new Excel.Application();
                oWB = oXL.Workbooks.Open(filename);

                oSheet = oWB.Sheets[Sheetname];
                Console.WriteLine(oWB.Sheets[Sheetname]);

                oSheet.Cells[column][row] = Status;
                
                Console.WriteLine(Status);
                oWB.Save();
                oWB.Close();
                oXL.Quit();
                
               
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                if (oWB != null) {

                    oWB.Close();
                }
                else if (oXL != null)
                {
                    oXL.Quit();

                }
            }
          
        }

    }

  

    public class DataCollection
    {
        public int RowNumber { get; set; }
        public string ColumnName { get; set; }
        public string Cellvalue { get; set; }
        public int ColumnNumber { get; set; }
    }
   

}

