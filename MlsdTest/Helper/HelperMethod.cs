﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MlsdTest.Base;

using OpenQA.Selenium.Interactions;

namespace MlsdTest.Helper
{
    public  class HelperMethod
    {
        private static IWebDriver _driver;




        //------------------------------------------Wait Helper------------------------------------------//       
        public static IWebElement ExplicitWaitByXpathdriver(IWebDriver driver, string XpathLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                wait.PollingInterval = TimeSpan.FromMilliseconds(250);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.XPath(XpathLocator)));
                return searchElement;
                Console.WriteLine("Element wait and clicked");
            }
            catch (Exception e)
            {

                Console.WriteLine("Element wait and exeption");
                Debug.WriteLine("To long to appear in Dom" + e.Message);
                return null;

            }
        }

        public static IWebElement ExplicitWaitByXpath(IWebDriver driver, string XpathLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                wait.PollingInterval = TimeSpan.FromMilliseconds(250);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.XPath(XpathLocator)));
                return searchElement;

            }
            catch (Exception e)
            {
                Debug.WriteLine("To long to appear in Dom" + e.Message);
                return null;

            }
        }


        public static IWebElement ExplicitWaitByName(IWebDriver driver, string Name)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                wait.PollingInterval = TimeSpan.FromMilliseconds(250);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.Name(Name)));
                return searchElement;

            }
            catch (Exception e)
            {
                Debug.WriteLine("To long to appear in Dom" + e.Message);
                return null;

            }
        }

        public static IWebElement ExplicitWaitById(IWebDriver driver, string ID)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                wait.PollingInterval = TimeSpan.FromMilliseconds(250);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.Id(ID)));
                return searchElement;

            }
            catch (Exception e)
            {
                Debug.WriteLine("To long to appear in Dom" + e.Message);
                return null;

            }
        }
        public static IWebElement FluentWaitByName(IWebDriver driver, string NameLocator)
        {
            try
            {
                DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(driver);
                fluentWait.Timeout = TimeSpan.FromSeconds(5);
                fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
                fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = fluentWait.Until(x => x.FindElement(By.Name(NameLocator)));
                return searchElement;
            }
            catch (Exception e)
            {
                Debug.WriteLine("To long to appear in Dom" + e.Message);
                return null;

            }
        }
        public static IWebElement FluentWaitById(IWebDriver driver, string IDLocator)
        {
            try
            {
                DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(driver);
                fluentWait.Timeout = TimeSpan.FromSeconds(5);
                fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
                fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = fluentWait.Until(x => x.FindElement(By.Name(IDLocator)));
                return searchElement;
            }
            catch (Exception e)
            {
                Debug.WriteLine("To long to appear in Dom" + e.Message);
                return null;

            }
        }
        public static IWebElement FluentWaitByXpath(IWebDriver driver, string Xpath)
        {
            try
            {
                DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(driver);
                fluentWait.Timeout = TimeSpan.FromSeconds(5);
                fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
                fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = fluentWait.Until(x => x.FindElement(By.Name(Xpath)));
                return searchElement;
            }
            catch (Exception e)
            {
                Debug.WriteLine("To long to appear in Dom" + e.Message);
                return null;

            }
        }
        //------------------------------------------Selector Helper------------------------------------------//       

        public static void SelectByText(IWebElement element, string text, string elementName)
        {
            SelectElement oSelect = new SelectElement(element);
            oSelect.SelectByText(text);
            Console.WriteLine(text + " text selected on " + elementName);
        }

        public static void SelectByIndex(IWebElement element, int index, string elementName)
        {
            SelectElement oSelect = new SelectElement(element);
            oSelect.SelectByIndex(index);
            Console.WriteLine(index + " index selected on " + elementName);
        }

        public static void SelectByValue(IWebElement element, string text, string elementName)
        {
            SelectElement oSelect = new SelectElement(element);
            oSelect.SelectByValue(text);
            Console.WriteLine(text + " value selected on " + elementName);
        }

        //------------------------------------------Selenium Wait and Actions------------------------------------------//  


        public static bool IsDisplayed(IWebElement element, string elementName)
        {
            bool result;
            try
            {
                result = element.Displayed;
                Console.WriteLine(elementName + " is Displayed.");
            }
            catch (NoSuchElementException)
            {
                result = false;
                Console.WriteLine(elementName + " is not Displayed.");
            }

            return result;
        }

        public static void ClickOnIt(IWebElement element, string elementName)
        {
            element.Click();
            Console.WriteLine("Clicked on " + elementName);
        }


        public static string MethodTest(IWebElement element, IWebElement element2, string elementName)
        {
            if (element.Displayed)
            {

                elementName = element.Text;
                Console.WriteLine(element);
                return element.Text;
            }
            else if (element2.Displayed)
            {
                 elementName = element2.Text;
                Console.WriteLine(element);
                return element2.Text;

            }
            else
            {
                return null;

            }
        }

        //public static void DisplayWait(String element)
        //{
        //    IWebElement visible = driver.FindElement(By.XPath(element));
        //    if (visible.Displayed) {
        //        //continue;
        //    }
        //}


        //------------------------------------------Selenium Wait and Actions------------------------------------------//  




        public static void clickOnIt(IWebElement element, string elementName)
        {
            

            element.Click();
            Console.WriteLine("Clicked on " + elementName);
        }
        public static void EnterValue(IWebElement element, string value, string elementName)
        {


            element.SendKeys(value);
            Console.WriteLine(value + "Entered " + elementName);
        }


        public static IWebElement find_element(By by)
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));
            return wait.Until(d => d.FindElement(by));
        }
        public static bool elementPresent(By by)
        {
            if (find_element(by).Displayed)
            {
                return true;
            }
            return false;
        }

        public static bool linkPresent(IWebElement element)
        {
            if (element.Displayed)
            {
                return true;
                Console.WriteLine("Link is Present");
            }
            Console.WriteLine("Link is not Present");
            return false;
        }
        public static bool IsElementPresent(By by)
        {
            try
            {
                _driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public static string GetText(IWebElement element)
        {

            String getText = element.Text;
            return getText;
        }


        public void SplitStringMethod(IWebElement element , String Str)
        {
            char[] spearator = { ',', ' ' };

            // using the method 
            String[] strlist = Str.Split(spearator,
            StringSplitOptions.RemoveEmptyEntries);

            foreach (String s in strlist)
            {
           
                Console.WriteLine(s);

            }
        }
        public static void waitElementClickable(By by)
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(5000)).Until(ExpectedConditions.ElementToBeClickable(by));
            Console.WriteLine("waitingElement to be clickable ");


        }




        //------------------------------------------Selenium Expected Conditions------------------------------------------//
    }




}



