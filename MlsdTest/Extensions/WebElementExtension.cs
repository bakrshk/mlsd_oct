﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MlsdTest.Extensions
{
    public static class WebElementExtension
    {
        private static int RowCountt;
        
        public static void SelectDDL(this IWebElement element, string Text)
        {
            SelectElement ddl = new SelectElement(element);
            ddl.SelectByText(Text);
            Thread.Sleep(1000);

        }
        public static void Gettabledata(IWebElement selectDate,string date)
        {
            Thread.Sleep(1000);
            List<IWebElement> allRows = new List<IWebElement>(selectDate.FindElements(By.TagName("tr")));
            Thread.Sleep(1000);
            RowCountt= allRows.Count;
            for (int i = 0; i < RowCountt; i++)
            {
                
                List<IWebElement> tableData = new List<IWebElement>(allRows[i].FindElements(By.TagName("a")));
                Thread.Sleep(1000);
                foreach (var entry in tableData)
                {
                    if (entry.Text == date)
                        entry.Click();
                    break;
                }
            }
            Thread.Sleep(1000);
        }
        public static string GetText(IWebElement element)
        {
            return element.Text;
        }

        public static void ListElement(IWebElement element,string dropdownvalue)
        {
            Thread.Sleep(1000);
            List<IWebElement> countriesList = new List<IWebElement>(element.FindElements(By.TagName("li")));
            Thread.Sleep(1000);
            foreach (var entry in countriesList)
            {
                if (entry.Text == dropdownvalue)
                {
                    entry.Click();
                    break;
                }
            }
          


        }

      
    }
}
